﻿namespace BombGame
{
	public interface IDestructable
	{
		float Hp { get; set; }
		uint MaxHp { get; set; }
		
		void TakeDamage (float amount, Player attacker);
		void Death (Player attacker);
	}
}