using System.Collections.Generic;

namespace BombGame
{
	public static class LocalUserInfo
	{
		public static string username;
		public static Dictionary<string, uint> winsDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> winsInCurrentSeasonDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> lossesDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> lossesInCurrentSeasonDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> killsDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> killsInCurrentSeasonDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> deathsDict = new Dictionary<string, uint>();
		public static Dictionary<string, uint> deathsInCurrentSeasonDict = new Dictionary<string, uint>();
		public static Dictionary<string, float> skillDict = new Dictionary<string, float>();
		public static Dictionary<string, float> skillInCurrentSeasonDict = new Dictionary<string, float>();
		public static uint goalsMade;
		public static uint goalsMadeInCurrentSeason;
		public static uint goaledOn;
		public static uint goaledOnInCurrentSeason;
		public static uint lastMonthPlayed;
	}
}