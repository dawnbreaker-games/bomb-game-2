using TMPro;
using UnityEngine;

namespace BombGame
{
	public class LevelButton : MonoBehaviour
	{
		public TMP_Text text;
		public string levelName;
	}
}