using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace BombGame
{
	public class MainMenu : SingletonMonoBehaviour<MainMenu>
	{
		public TMP_InputField[] minPlayersInputFields = new TMP_InputField[0];
		public TMP_InputField[] maxPlayersInputFields = new TMP_InputField[0];
		public Toggle[] rankedToggles = new Toggle[0];
		public TMP_InputField usernameInputField;
		public TMP_InputField passwordInputField;
		public Toggle rememberLoginInfoToggle;
		public TMP_InputField[] matchIdInputFields = new TMP_InputField[0];
		public Selectable[] registerAndLoginSelectables = new Selectable[0];
		public Button[] submitButtons = new Button[0];
		public GameObject[] deactivateGosOnLogin = new GameObject[0];
		public GameObject activateGoOnLogin;
		public static bool isPrivate;
		public static int gameModeIndex;
		public static string gameModeName;
		public static int[] MinPlayers
		{
			get
			{
				return SaveAndLoadManager.GetIntArray("Min players", new int[] { 1, 1 });
			}
			set
			{
				SaveAndLoadManager.SetIntArray ("Min players", value);
			}
		}
		public static int[] MaxPlayers
		{
			get
			{
				return SaveAndLoadManager.GetIntArray("Max players", new int[] { 4, 4 });
			}
			set
			{
				SaveAndLoadManager.SetIntArray ("Max players", value);
			}
		}
		public static bool[] Ranked
		{
			get
			{
				return SaveAndLoadManager.GetBoolArray("Ranked", new bool[2]);
			}
			set
			{
				SaveAndLoadManager.SetBoolArray ("Ranked", value);
			}
		}
		public static bool IsPrivate
		{
			get
			{
				return isPrivate;
			}
			set
			{
				isPrivate = value;
			}
		}
		public static string[] MatchIds
		{
			get
			{
				return SaveAndLoadManager.GetStringArray("Match ids", new string[2]);
			}
			set
			{
				SaveAndLoadManager.SetStringArray ("Match ids", value);
			}
		}
		public static string RememberedUsername
		{
			get
			{
				return SaveAndLoadManager.GetString("Username");
			}
			set
			{
				SaveAndLoadManager.SetString ("Username", value);
			}
		}
		public static string RememberedPassword
		{
			get
			{
				return SaveAndLoadManager.GetString("Password");
			}
			set
			{
				SaveAndLoadManager.SetString ("Password", value);
			}
		}
		public static bool RememberLoginInfo
		{
			get
			{
				return SaveAndLoadManager.GetBool("Remember login info", false);
			}
			set
			{
				SaveAndLoadManager.SetBool ("Remember login info", value);
			}
		}
		static string password;
		static string[] usernames = new string[0];
		static DatabaseObject dbObj;

		public override void Awake ()
		{
			base.Awake ();
			for (int i = 0; i < minPlayersInputFields.Length; i ++)
			{
				minPlayersInputFields[i].text = "" + MinPlayers[i];
				maxPlayersInputFields[i].text = "" + MaxPlayers[i];
				rankedToggles[i].isOn = Ranked[i];
				matchIdInputFields[i].text = MatchIds[i];
			}
			if (RememberLoginInfo)
			{
				usernameInputField.text = RememberedUsername;
				passwordInputField.text = RememberedPassword;
			}
			rememberLoginInfoToggle.isOn = RememberLoginInfo;
		}

		public void SetGameMode (int gameModeIndex)
		{
			MainMenu.gameModeIndex = gameModeIndex;
			gameModeName = LeaderboardMenu._gameModeTypes[gameModeIndex].name;
		}

		public void SetMinPlayers (TMP_InputField inputField)
		{
			string text = GetInputFieldText(inputField);
			MinPlayers[gameModeIndex] = int.Parse(text);
			if (MinPlayers[gameModeIndex] == 0)
			{
				MinPlayers[gameModeIndex] = 1;
				inputField.text = "" + MinPlayers[gameModeIndex];
			}
			else if (MinPlayers[gameModeIndex] > MaxPlayers[gameModeIndex])
			{
				MinPlayers[gameModeIndex] = MaxPlayers[gameModeIndex];
				inputField.text = "" + MinPlayers[gameModeIndex];
			}
			if (MinPlayers[gameModeIndex] != 2)
				rankedToggles[gameModeIndex].isOn = false;
		}

		public void SetMaxPlayers (TMP_InputField inputField)
		{
			string text = GetInputFieldText(inputField);
			MaxPlayers[gameModeIndex] = int.Parse(text);
			if (MaxPlayers[gameModeIndex] == 0)
			{
				MaxPlayers[gameModeIndex] = 1;
				inputField.text = "" + MaxPlayers[gameModeIndex];
			}
			else if (MaxPlayers[gameModeIndex] < MinPlayers[gameModeIndex])
			{
				MaxPlayers[gameModeIndex] = MinPlayers[gameModeIndex];
				inputField.text = "" + MaxPlayers[gameModeIndex];
			}
			if (MaxPlayers[gameModeIndex] != 2)
				rankedToggles[gameModeIndex].isOn = false;
		}

		public void SetRanked (bool ranked)
		{
			Ranked[gameModeIndex] = ranked;
			if (ranked)
			{
				minPlayersInputFields[gameModeIndex].text = "2";
				maxPlayersInputFields[gameModeIndex].text = "2";
			}
		}

		public void SetUsername (string username)
		{
			if (RememberLoginInfo)
				RememberedUsername = username;
			LocalUserInfo.username = username;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetPassword (string password)
		{
			if (RememberLoginInfo)
				RememberedPassword = password;
			MainMenu.password = password;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(LocalUserInfo.username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetRememberLoginInfo (bool remember)
		{
			RememberLoginInfo = remember;
			if (remember)
			{
				RememberedUsername = LocalUserInfo.username;
				RememberedPassword = password;
			}
		}

		public void Register ()
		{
			rankedToggles[gameModeIndex].interactable = false;
			rankedToggles[gameModeIndex].isOn = false;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty username!");
				return;
			}
			else if (string.IsNullOrEmpty(password))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty password!");
				return;
			}
			Logout ();
			NetworkManager.Connect (OnConnectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnConnectSuccess_Register (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			NetworkManager.client.BigDB.LoadOrCreate("Usernames", "usernames", OnLoadUsernamesDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnLoadUsernamesDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			DatabaseArray usernamesDBArray = dbObj.GetArray("usernames");
			if (!usernamesDBArray.Contains(LocalUserInfo.username))
				usernamesDBArray.Add(LocalUserInfo.username);
			else
			{
				GameManager.instance.DisplayNotification ("An account with that username already exists!");
				return;
			}
			MainMenu.dbObj = dbObj;
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnSaveDBObjectSuccess_Register ()
		{
			DatabaseObject dbObj2 = new DatabaseObject();
			dbObj2.Set("password", password);
			DatabaseArray dbArray = new DatabaseArray();
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
				dbArray.Add((float) 0);
			dbObj2.Set("skill", dbArray);
			dbObj2.Set("skillInCurrentSeason", dbArray);
			dbArray = new DatabaseArray();
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
				dbArray.Add((uint) 0);
			dbObj2.Set("wins", dbArray);
			dbObj2.Set("winsInCurrentSeason", dbArray);
			dbObj2.Set("losses", dbArray);
			dbObj2.Set("lossesInCurrentSeason", dbArray);
			dbObj2.Set("kills", dbArray);
			dbObj2.Set("killsInCurrentSeason", dbArray);
			dbObj2.Set("deaths", dbArray);
			dbObj2.Set("deathsInCurrentSeason", dbArray);
			dbObj2.Set("survivedTimeInSurvival", dbArray);
			dbObj2.Set("goalsMade", (uint) 0);
			dbObj2.Set("goalsMadeInCurrentSeason", (uint) 0);
			dbObj2.Set("goaledOn", (uint) 0);
			dbObj2.Set("goaledOnInCurrentSeason", (uint) 0);
			dbObj2.Set("survivedTimeInSurvival", (uint) 0);
			dbObj2.Set("lastMonthPlayed", (uint) DateTime.UtcNow.Month);
			dbObj2.Set("friends", new DatabaseArray());
			dbObj2.Set("otherPartyMembers", new DatabaseArray());
			NetworkManager.client.BigDB.CreateObject("PlayerObjects", LocalUserInfo.username, dbObj2, OnCreatePlayerDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnSaveDBObjectFail_Register (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnCreatePlayerDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			activateGoOnLogin.SetActive(true);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
			rankedToggles[gameModeIndex].interactable = true;
			GameManager.updatables = GameManager.updatables.Add(LeaderboardMenu.resetLeaderboardUpdater);
		}

		void OnRegisterOrLoginFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
		}

		public void Login ()
		{
			rankedToggles[gameModeIndex].interactable = false;
			rankedToggles[gameModeIndex].isOn = false;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty username!");
				return;
			}
			else if (string.IsNullOrEmpty(password))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty password!");
				return;
			}
			Logout ();
			NetworkManager.Connect (OnConnectSuccess_Login, OnRegisterOrLoginFail);
		}

		public void OnConnectSuccess_Login (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadPlayerDBObjectSuccess_Login, OnRegisterOrLoginFail);
		}

		void OnLoadPlayerDBObjectSuccess_Login (DatabaseObject dbObj)
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
			if (dbObj.GetString("password") != password)
			{
				GameManager.instance.DisplayNotification ("Incorrect login info.");
				return;
			}
			DatabaseArray dbArray = dbObj.GetArray("skill");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.skillDict[gameModeType.name] = dbArray.GetFloat(gameModeType.name);
			}
			dbArray = dbObj.GetArray("skillInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.skillInCurrentSeasonDict[gameModeType.name] = dbArray.GetFloat(gameModeType.name);
			}
			dbArray = dbObj.GetArray("wins");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.winsDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("winsInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.winsInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("losses");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.lossesDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("lossesInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.lossesInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("kills");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.killsDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("killsInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.killsInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("deaths");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.deathsDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("deathsInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.deathsInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			LocalUserInfo.goalsMade = dbObj.GetUInt("goalsMade");
			LocalUserInfo.goalsMadeInCurrentSeason = dbObj.GetUInt("goalsMadeInCurrentSeason");
			LocalUserInfo.goaledOn = dbObj.GetUInt("goaledOn");
			LocalUserInfo.goaledOnInCurrentSeason = dbObj.GetUInt("goaledOnInCurrentSeason");
			LocalUserInfo.lastMonthPlayed = dbObj.GetUInt("lastMonthPlayed");
			DatabaseArray friendsDBArray = dbObj.GetArray("friends");
			FriendsMenu.friendsUsernames.Clear();
			for (int i = 0; i < friendsDBArray.Count; i ++)
			{
				string friend = friendsDBArray.GetString(i);
				FriendsMenu.friendsUsernames.Add(friend);
			}
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			activateGoOnLogin.SetActive(true);
			rankedToggles[gameModeIndex].interactable = true;
			GameManager.updatables = GameManager.updatables.Add(LeaderboardMenu.resetLeaderboardUpdater);
		}

		void Logout ()
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = false;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = false;
			}
			FriendsMenu.friendsUsernames.Clear();
			GameManager.updatables = GameManager.updatables.Remove(LeaderboardMenu.resetLeaderboardUpdater);
			NetworkManager.Disconnect ();
		}

		string GetInputFieldText (TMP_InputField inputField)
		{
			string output = inputField.text;
			if (string.IsNullOrEmpty(output))
				output = ((TMP_Text) inputField.placeholder).text;
			return output;
		}
	}
}