using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace BombGame
{
	public class LeaderboardMenu : SingletonMonoBehaviour<LeaderboardMenu>
	{
		public Toggle rankedToggle;
		public Toggle showOnlyCurrentSeasonToggle;
		public Toggle showAllLeaderboardEntriesToggle;
		public Toggle showOnlyYouAndFriendsLeaderboardEntriesToggle;
		public LeaderboardEntry leaderboardEntryPrefab;
		public Transform leaderboardEntriesParent;
		public uint maxLeaderboardEntriesPerPage;
		public _Text leaderboardPageText;
		public GameModeType[] gameModeTypes = new GameModeType[0];
		public Button[] setValueTypeButtons = new Button[0];
		public static bool _ShowAllLeaderboardEntries
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show all leaderboard entries", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Show all leaderboard entries", value);
			}
		}
		public static bool _ShowOnlyYouAndFriendLeaderboardEntries
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show only you and friend leaderboard entries", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Show only you and friend leaderboard entries", value);
			}
		}
		public static ResetLeaderboardUpdater resetLeaderboardUpdater = new ResetLeaderboardUpdater();
		public static GameModeType[] _gameModeTypes = new GameModeType[0];
		static GameModeType gameModeType;
		static LeaderboardEntry.ValueType leaderboardValueType;
		static string[] usernames = new string[0];
		static DatabaseObject dbObj;
		static bool leaderboardIsOpen;
		static bool showCurrentSeasonLeaderboardInfo;
		static bool showAllLeaderboardEntries;
		static bool showOnlyYouAndFriendLeaderboardEntries;
		static uint leaderboardPage;

		public override void Awake ()
		{
			base.Awake ();
			gameModeType = gameModeTypes[0];
			_gameModeTypes = new GameModeType[gameModeTypes.Length];
			gameModeTypes.CopyTo(_gameModeTypes, 0);
			showAllLeaderboardEntriesToggle.isOn = _ShowAllLeaderboardEntries;
			showOnlyCurrentSeasonToggle.isOn = _ShowOnlyYouAndFriendLeaderboardEntries;
			gameObject.SetActive(false);
		}

		void UnloadLeaderboard ()
		{
			leaderboardIsOpen = false;
			if (leaderboardEntriesParent == null)
				return;
			for (int i = 0; i < leaderboardEntriesParent.childCount; i ++)
			{
				Transform child = leaderboardEntriesParent.GetChild(i);
				Destroy(child.gameObject);
			}
		}

		void LoadLeaderboard ()
		{
			leaderboardIsOpen = true;
			leaderboardPage = 0;
			if (leaderboardPageText != null)
				leaderboardPageText.Text = "Page 1 / " + Mathf.Clamp(usernames.Length / maxLeaderboardEntriesPerPage, 1, uint.MaxValue);
			NetworkManager.client.BigDB.Load("Usernames", "usernames", OnLoadDBObjectSuccess_LoadLeaderboard, OnLoadDBObjectFail_LoadLeaderboard);
		}

		public void ReloadLeaderboard ()
		{
			if (NetworkManager.client == null)
			{
				NetworkManager.Connect (OnConnectSuccess_ReloadLeaderboard, NetworkManager.DisplayError);
				return;
			}
			UnloadLeaderboard ();
			LoadLeaderboard ();
		}

		void OnConnectSuccess_ReloadLeaderboard (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			ReloadLeaderboard ();
		}

		public void SetLeaderboardValueType (int valueTypeIndex)
		{
			leaderboardValueType = (LeaderboardEntry.ValueType) Enum.ToObject(typeof(LeaderboardEntry.ValueType), valueTypeIndex);
			ReloadLeaderboard ();
		}

		void OnLoadDBObjectSuccess_LoadLeaderboard (DatabaseObject dbObj)
		{
			DatabaseArray dbArray = dbObj.GetArray("usernames");
			if (dbArray.Count == 0)
				return;
			usernames = new string[dbArray.Count];
			for (int i = 0; i < dbArray.Count; i ++)
				usernames[i] = dbArray.GetString(i);
			NetworkManager.client.BigDB.LoadKeys("PlayerObjects", usernames, OnLoadDBObjectsSuccess, OnLoadDBObjectsFail);
		}
		
		void OnLoadDBObjectsSuccess (DatabaseObject[] dbObjs)
		{
			SortedDictionary<float, List<string>> leaderboardValuesDict = new SortedDictionary<float, List<string>>(new ValueComparer());
			for (int i = 0; i < dbObjs.Length; i ++)
			{
				DatabaseObject dbObj = dbObjs[i];
				uint lastMonthPlayed = dbObj.GetUInt("lastMonthPlayed");
				if ((uint) ((float) lastMonthPlayed / 3) % 4 == (uint) ((float) LocalUserInfo.lastMonthPlayed / 3) % 4) // Checks if the user has played in the same season as the local user
				{
					float value = 0;
					if (showCurrentSeasonLeaderboardInfo)
					{
						if (leaderboardValueType == LeaderboardEntry.ValueType.Skill)
							value = dbObj.GetArray("skillInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Wins)
							value = dbObj.GetArray("winsInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Losses)
							value = dbObj.GetArray("lossesInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.WinLossRatio)
							value = (float) dbObj.GetArray("winsInCurrentSeason").GetUInt(gameModeType.name) / dbObj.GetArray("lossesInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Kills)
							value = dbObj.GetArray("killsInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Deaths)
							value = dbObj.GetArray("deathsInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.KillDeathRatio)
							value = (float) dbObj.GetArray("killsInCurrentSeason").GetUInt(gameModeType.name) / dbObj.GetArray("deathsInCurrentSeason").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.GoalsMade)
							value = dbObj.GetUInt("goalsMadeInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.GoaledOn)
							value = dbObj.GetUInt("goaledOnInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.GoalsMadeVsGoaledOnRatio)
							value = (float) dbObj.GetUInt("goalsMadeInCurrentSeason") / dbObj.GetUInt("goaledOnInCurrentSeason");
						else// if (leaderboardValueType == LeaderboardEntry.ValueType.SurvivedTimeInSurvival)
							value = dbObj.GetUInt("survivedTimeInSurvival");
					}
					else
					{
						if (leaderboardValueType == LeaderboardEntry.ValueType.Skill)
							value = dbObj.GetArray("skill").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Wins)
							value = dbObj.GetArray("wins").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Losses)
							value = dbObj.GetArray("losses").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.WinLossRatio)
							value = (float) dbObj.GetArray("wins").GetUInt(gameModeType.name) / dbObj.GetArray("losses").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Kills)
							value = dbObj.GetArray("kills").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Deaths)
							value = dbObj.GetArray("deaths").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.KillDeathRatio)
							value = (float) dbObj.GetArray("kills").GetUInt(gameModeType.name) / dbObj.GetArray("deaths").GetUInt(gameModeType.name);
						else if (leaderboardValueType == LeaderboardEntry.ValueType.GoalsMade)
							value = dbObj.GetUInt("goalsMade");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.GoaledOn)
							value = dbObj.GetUInt("goaledOn");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.GoalsMadeVsGoaledOnRatio)
							value = (float) dbObj.GetUInt("goalsMade") / dbObj.GetUInt("goaledOn");
						else// if (leaderboardValueType == LeaderboardEntry.ValueType.SurvivedTimeInSurvival)
							value = dbObj.GetUInt("survivedTimeInSurvival");
					}
					List<string> usernamesWithValue = new List<string>();
					if (leaderboardValuesDict.TryGetValue(value, out usernamesWithValue))
						usernamesWithValue.Add(usernames[i]);
					else
						usernamesWithValue = new List<string>(new string[] { usernames[i] });
					leaderboardValuesDict[value] = usernamesWithValue;
				}
			}
			uint rank = 1;
			uint entryIndex = 0;
			foreach (KeyValuePair<float, List<string>> keyValuePair in leaderboardValuesDict)
			{
				for (int i = 0; i < keyValuePair.Value.Count; i ++)
				{
					if (entryIndex >= leaderboardPage * maxLeaderboardEntriesPerPage)
					{
						string username = keyValuePair.Value[i];
						if (showAllLeaderboardEntries || (showOnlyYouAndFriendLeaderboardEntries && FriendsMenu.friendsUsernames.Contains(username)) || username == LocalUserInfo.username)
						{
							LeaderboardEntry leaderboardEntry = Instantiate(leaderboardEntryPrefab, leaderboardEntriesParent);
							leaderboardEntry.rankText.Text = "" + rank;
							leaderboardEntry.usernameText.Text = username;
							leaderboardEntry.valueText.Text = "" + keyValuePair.Key.ToString("F0");
							leaderboardEntry.valueTypeIndicators[leaderboardValueType.GetHashCode()].SetActive(true);
						}
					}
					entryIndex ++;
					if (entryIndex > leaderboardPage * maxLeaderboardEntriesPerPage + maxLeaderboardEntriesPerPage)
						return;
				}
				rank ++;
				if (rank > maxLeaderboardEntriesPerPage)
					return;
			}
		}

		void OnLoadDBObjectsFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.client.BigDB.LoadKeys("PlayerObjects", usernames, OnLoadDBObjectsSuccess, OnLoadDBObjectsFail);
		}

		void OnLoadDBObjectFail_LoadLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.client.BigDB.Load("Usernames", "usernames", OnLoadDBObjectSuccess_LoadLeaderboard, OnLoadDBObjectFail_LoadLeaderboard);
		}

		static void ResetLeaderboard ()
		{
			if (NetworkManager.client == null)
			{
				NetworkManager.Connect (OnConnectSuccess_ResetLeaderboard, NetworkManager.DisplayError);
				return;
			}
			NetworkManager.client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess_ResetLeaderboard, OnLoadDBObjectFail_ResetLeaderboard);
		}

		static void OnConnectSuccess_ResetLeaderboard (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			ResetLeaderboard ();
		}

		static void OnLoadDBObjectSuccess_ResetLeaderboard (DatabaseObject dbObj)
		{
			LeaderboardMenu.dbObj = dbObj;
			DatabaseArray dbArray = new DatabaseArray();
			for (int i = 0; i < _gameModeTypes.Length; i ++)
				dbArray.Add((float) 0);
			dbObj.Set("skillInCurrentSeason", dbArray);
			LocalUserInfo.skillInCurrentSeasonDict.Clear();
			dbArray = new DatabaseArray();
			for (int i = 0; i < _gameModeTypes.Length; i ++)
				dbArray.Add((uint) 0);
			dbObj.Set("winsInCurrentSeason", dbArray);
			LocalUserInfo.winsInCurrentSeasonDict.Clear();
			dbObj.Set("lossesInCurrentSeason", dbArray);
			LocalUserInfo.lossesInCurrentSeasonDict.Clear();
			dbObj.Set("killsInCurrentSeason", dbArray);
			LocalUserInfo.killsInCurrentSeasonDict.Clear();
			dbObj.Set("deathsInCurrentSeason", dbArray);
			LocalUserInfo.deathsInCurrentSeasonDict.Clear();
			dbObj.Set("goalsMadeInCurrentSeason", (uint) 0);
			LocalUserInfo.goalsMadeInCurrentSeason = (uint) 0;
			dbObj.Set("goaledOnInCurrentSeason", (uint) 0);
			LocalUserInfo.goaledOnInCurrentSeason = (uint) 0;
			LocalUserInfo.lastMonthPlayed = (uint) DateTime.UtcNow.Month;
			dbObj.Set("lastMonthPlayed", LocalUserInfo.lastMonthPlayed);
			dbObj.Save(OnSaveDBObjectSuccess_ResetLeaderboard, OnSaveDBObjectFail_ResetLeaderboard);
		}

		static void OnLoadDBObjectFail_ResetLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess_ResetLeaderboard, OnLoadDBObjectFail_ResetLeaderboard);
		}

		static void OnSaveDBObjectSuccess_ResetLeaderboard ()
		{
			if (instance == null)
				return;
			bool previouslyOpen = leaderboardIsOpen;
			instance.UnloadLeaderboard ();
			if (previouslyOpen)
				instance.LoadLeaderboard ();
		}

		static void OnSaveDBObjectFail_ResetLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			dbObj.Save(OnSaveDBObjectSuccess_ResetLeaderboard, OnSaveDBObjectFail_ResetLeaderboard);
		}

		public void ShowCurrentSeasonLeaderboardInfo (bool showCurrentSeasonLeaderboardInfo)
		{
			LeaderboardMenu.showCurrentSeasonLeaderboardInfo = showCurrentSeasonLeaderboardInfo;
			ReloadLeaderboard ();
		}

		public void ShowAllLeaderboardEntries (bool showAllLeaderboardEntries)
		{
			LeaderboardMenu.showAllLeaderboardEntries = showAllLeaderboardEntries;
			if (showAllLeaderboardEntries)
				showOnlyYouAndFriendsLeaderboardEntriesToggle.SetIsOnWithoutNotify(false);
			ReloadLeaderboard ();
		}

		public void ShowOnlyYouAndFriendLeaderboardEntries (bool showOnlyYouAndFriendLeaderboardEntries)
		{
			LeaderboardMenu.showOnlyYouAndFriendLeaderboardEntries = showOnlyYouAndFriendLeaderboardEntries;
			if (showOnlyYouAndFriendLeaderboardEntries)
				showAllLeaderboardEntriesToggle.SetIsOnWithoutNotify(false);
			ReloadLeaderboard ();
		}

		public void NextLeaderboardPage ()
		{
			if (leaderboardPage < usernames.Length / maxLeaderboardEntriesPerPage)
				leaderboardPage ++;
			else
				leaderboardPage = 0;
			leaderboardPageText.Text = "Page " + (leaderboardPage + 1) + " / " + Mathf.Clamp(usernames.Length / maxLeaderboardEntriesPerPage, 1, uint.MaxValue);
			ReloadLeaderboard ();
		}

		public void PreviousLeaderboardPage ()
		{
			if (leaderboardPage > 0)
				leaderboardPage --;
			else
				leaderboardPage = (uint) (usernames.Length / maxLeaderboardEntriesPerPage);
			leaderboardPageText.Text = "Page " + (leaderboardPage + 1) + " / " + Mathf.Clamp(usernames.Length / maxLeaderboardEntriesPerPage, 1, uint.MaxValue);
			ReloadLeaderboard ();
		}

		public void SetGameModeType (int gameModeTypeIndex)
		{
			gameModeType = gameModeTypes[gameModeTypeIndex];
			Array allValueTypes = Enum.GetValues(typeof(LeaderboardEntry.ValueType));
			LeaderboardEntry.ValueType[] valueTypes = gameModeType.valueTypeGroup.valueTypes;
			for (int i = 0; i < allValueTypes.Length; i ++)
				setValueTypeButtons[i].gameObject.SetActive(false);
			for (int i = 0; i < valueTypes.Length; i ++)
			{
				LeaderboardEntry.ValueType valueType = valueTypes[i];
				setValueTypeButtons[allValueTypes.IndexOf(valueType)].gameObject.SetActive(true);
			}
		}

		void OnApplicationQuit ()
		{
			GameManager.updatables = GameManager.updatables.Remove(resetLeaderboardUpdater);
		}

		class ValueComparer : IComparer<float>
		{
			public int Compare (float value, float value2)
			{
				int multiplier = 1;
				if (leaderboardValueType == LeaderboardEntry.ValueType.Losses || leaderboardValueType == LeaderboardEntry.ValueType.Deaths || leaderboardValueType == LeaderboardEntry.ValueType.GoaledOn)
					multiplier *= -1;
				if (value > value2)
					return -multiplier;
				else if (value < value2)
					return multiplier;
				else
					return 0;
			}
		}

		public class ResetLeaderboardUpdater : IUpdatable
		{
			public void DoUpdate ()
			{
				uint month = (uint) DateTime.UtcNow.Month;
				if (month != LocalUserInfo.lastMonthPlayed)
				{
					LocalUserInfo.lastMonthPlayed = month;
					if (month % 3 == 0)
						ResetLeaderboard ();
				}
			}
		}

		[Serializable]
		public class GameModeType
		{
			public string name;
			public ValueTypeGroup valueTypeGroup;

			public override string ToString ()
			{
				return name;
			}

			[Serializable]
			public class ValueTypeGroup
			{
				public LeaderboardEntry.ValueType[] valueTypes = new LeaderboardEntry.ValueType[0];
			}
		}
	}
}