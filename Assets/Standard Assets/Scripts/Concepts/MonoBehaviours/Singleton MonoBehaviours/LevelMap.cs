using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace BombGame
{
	public class LevelMap : SingletonMonoBehaviour<LevelMap>
	{
		public Transform trs;
		public Camera camera;
		public static string previousLevelName;
		public static Rect boundsRect;
		
		public override void Awake ()
		{
			base.Awake ();
			if (string.IsNullOrEmpty(previousLevelName))
				previousLevelName = SceneManager.GetSceneByBuildIndex(LevelSelect.firstLevelBuildIndex).name;
		}

		public void Make (string levelName)
		{
			if (!string.IsNullOrEmpty(previousLevelName))
				_SceneManager.Instance.UnloadSceneAsync (previousLevelName);
			GameManager.paused = true;
			AsyncOperation levelLoader = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
			GameManager.updatables = GameManager.updatables.Add(new LevelLoader(levelLoader, levelName));
		}
		
		public static Bounds GetBounds ()
		{
			List<Bounds> levelBoundsInstances = new List<Bounds>();
			Renderer[] renderers = FindObjectsOfType<Renderer>();
			for (int i = 0; i < renderers.Length; i ++)
			{
				Renderer renderer = renderers[i];
				// if (renderer.GetComponent<OmitFromLevelMap>() == null)
					levelBoundsInstances.Add(renderer.bounds);
			}
			return BoundsExtensions.Combine(levelBoundsInstances.ToArray());
		}
		
		public static Rect GetBoundsRect ()
		{
			List<Rect> levelBoundRectsInstances = new List<Rect>();
			Renderer[] renderers = FindObjectsOfType<Renderer>();
			for (int i = 0; i < renderers.Length; i ++)
			{
				Renderer renderer = renderers[i];
				// if (renderer.GetComponent<OmitFromLevelMap>() == null)
					levelBoundRectsInstances.Add(renderer.bounds.ToRect());
			}
			return RectExtensions.Combine(levelBoundRectsInstances.ToArray());
		}

		public class LevelLoader : IUpdatable
		{
			string levelName;
			AsyncOperation levelLoader;

			public LevelLoader (AsyncOperation levelLoader, string levelName)
			{
				this.levelLoader = levelLoader;
				this.levelName = levelName;
			}

			public void DoUpdate ()
			{
				if (levelLoader.isDone)
				{
					if (!string.IsNullOrEmpty(previousLevelName) && previousLevelName != levelName)
						_SceneManager.instance.UnloadSceneAsync (previousLevelName);
					Bounds levelBounds = GetBounds();
					instance.trs.position = levelBounds.center.SetZ(instance.trs.position.z);
					instance.camera.orthographicSize = Mathf.Max(levelBounds.size.x, levelBounds.size.y) / 2;
				}
			}
		} 
	}
}