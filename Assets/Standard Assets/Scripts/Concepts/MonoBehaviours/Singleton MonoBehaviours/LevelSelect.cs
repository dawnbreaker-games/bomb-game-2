using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BombGame
{
	public class LevelSelect : SingletonMonoBehaviour<LevelSelect>
	{
		public Transform trs;
		public int _firstLevelBuildIndex;
		public LevelButton levelButtonPrefab;
		public Text levelTitle;
		public LevelButton[] levelButtons;
		public static int firstLevelBuildIndex;
		public static int PreviousLevelIndex
		{
			get
			{
				return SaveAndLoadManager.GetInt("Previous level", 0);
			}
			set
			{
				SaveAndLoadManager.SetInt ("Previous level", value);
			}
		}
		
#if UNITY_EDITOR
		public bool addNewButton;
		
		void OnValidate ()
		{
			if (addNewButton)
			{
				addNewButton = false;
				LevelButton button = Instantiate(levelButtonPrefab, trs);
				Transform buttonTrs = button.GetComponent<Transform>();
				buttonTrs.localScale = Vector3.one;
				EditorBuildSettingsScene scene = EditorBuildSettings.scenes[_firstLevelBuildIndex + buttonTrs.GetSiblingIndex()];
				string sceneName = scene.path.Substring(scene.path.LastIndexOf("/") + 1).Replace(".unity", "");
				button.name = sceneName;
				button.levelName = sceneName;
				button.text.text = sceneName;
			}
		}
#endif
		
		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			firstLevelBuildIndex = _firstLevelBuildIndex;
			LevelMap.previousLevelName = "";
			LevelButton previousLevelButton = levelButtons[PreviousLevelIndex];
			EventSystem.current.SetSelectedGameObject(previousLevelButton.gameObject);
		}
	}
}