using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace BombGame
{
	public class InviteRequest : MonoBehaviour
	{
		public CanvasGroup canvasGroup;
		public _Text text;
		int gameModeIndex;
		string inviterUsername;
		string[] otherPartyMembersUsernames = new string[0];
		const string REPLACE_STRING = "_";

		public void Init (int gameModeIndex, string inviterUsername, string[] otherPartyMembersUsernames)
		{
			this.gameModeIndex = gameModeIndex;
			this.inviterUsername = inviterUsername;
			this.otherPartyMembersUsernames = otherPartyMembersUsernames;
			InviteFriendsMenu.localPendingInvites.Add(inviterUsername);
			text.Text = text.Text.Replace(REPLACE_STRING, inviterUsername);
		}

		public void Reject ()
		{
			InviteFriendsMenu.localRejectedInvites.Add(inviterUsername);
			Destroy(gameObject);
		}

		public void Accept ()
		{
			canvasGroup.interactable = false;
			if (gameModeIndex != -1)
			{
				MainMenu.isPrivate = true;
				MainMenu.gameModeIndex = gameModeIndex;
				string gameModeName = LeaderboardMenu._gameModeTypes[gameModeIndex].name;
				MainMenu.MatchIds[MainMenu.gameModeIndex] = inviterUsername;
				_SceneManager.instance.LoadScene (gameModeName);
			}
			else
			{
				AddPartyMember (inviterUsername);
				for (int i = 0; i < otherPartyMembersUsernames.Length; i ++)
					AddPartyMember (otherPartyMembersUsernames[i]);
				NetworkManager.client.GameRequests.Send("invitefriend", new Dictionary<string, string>() { { "", "" } }, new string[] { inviterUsername }, null, NetworkManager.DisplayError);
				Destroy(gameObject);
			}
		}

		void OnDestroy ()
		{
			InviteFriendsMenu.localPendingInvites.Remove(inviterUsername);
		}

		public static void AddPartyMember (string inviterUsername)
		{
			PartyMemberInfoIndicator partyMemberInfoIndicator = Instantiate(InviteFriendsMenu.instance.partyMemberInfoIndicatorPrefab, InviteFriendsMenu.instance.partyMemberInfoIndicatorsParent);
			partyMemberInfoIndicator.Init (inviterUsername);
			InviteFriendsMenu.otherPartyMemberCount ++;
		}
	}
}