using UnityEngine;
using UnityEngine.UI;

namespace BombGame
{
	public class ScoreboardEntry : MonoBehaviour
	{
		public Transform trs;
		public _Text scoreText;
		public Image teamColorIndicator;

		public void Init (Match.Team team)
		{
			teamColorIndicator.color = team.color;
		}
	}
}