using UnityEngine;

namespace BombGame
{
	public class ObjectInWorld : MonoBehaviour
	{
		public Transform trs;
		// [HideInInspector]
		public WorldPiece pieceIAmIn;
		// [HideInInspector]
		public ObjectInWorld duplicate;
		public ObjectInWorld[] objectsToLoadAndUnloadWithMe = new ObjectInWorld[0];
		public bool dontUnloadAfterLoad;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif

		void OnEnable ()
		{
			if (dontUnloadAfterLoad)
				trs.SetParent(null);
		}
	}
}