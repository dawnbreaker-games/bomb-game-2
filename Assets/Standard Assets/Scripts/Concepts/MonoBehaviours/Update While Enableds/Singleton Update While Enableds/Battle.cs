using System;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections.Generic;

namespace BombGame
{
	public class Battle : Match
	{
		public new static Battle instance;
		public new static Battle Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Battle>();
				return instance;
			}
			set
			{
				instance = value;
			}
		}
	}
}