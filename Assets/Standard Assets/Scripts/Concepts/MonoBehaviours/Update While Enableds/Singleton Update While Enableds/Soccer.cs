using System;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace BombGame
{
	public class Soccer : Match
	{
		public SerializableDictionary<Match.Team, SoccerGoal> teamGoalsDict = new SerializableDictionary<Match.Team, SoccerGoal>();
		public SerializableDictionary<Match.Team, SpawnPointGroup> teamSpawnTransformsDict = new SerializableDictionary<Match.Team, SpawnPointGroup>();
		public float countdownDuration;
		public _Text countdownText;
		public Transform countdownBar;
		public Match.Team[] teams = new Match.Team[0];
		public new static Soccer instance;
		public new static Soccer Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Soccer>();
				return instance;
			}
			set
			{
				instance = value;
			}
		}
		Vector2 ballSpawnPosition;

		public override void Awake ()
		{
			base.Awake ();
			teamGoalsDict.keys.AddRange(teams);
			teamGoalsDict.values[0].owner = teams[0];
			teamGoalsDict.values[1].owner = teams[1];
			teamGoalsDict.Init ();
			teamSpawnTransformsDict.keys.AddRange(teams);
			teamSpawnTransformsDict.Init ();
			ballSpawnPosition = Soccerball.Instance.trs.position;
		}

		protected override Player OnSpawnPlayerMessage (Message message)
		{
			Player player = base.OnSpawnPlayerMessage(message);
			Scoreboard.instance.RemoveEntry (player.owner);
			Match.Team team = teams[player.id % 2];
			player.owner = team;
			team.representatives = team.representatives.Add(player);
			ApplyTeam (player);
			Scoreboard.instance.AddEntry (player.owner);
			return player;
		}

		protected override void ReviveLocalPlayer (uint id)
		{
			Match.Team team = teams[id % 2];
			Transform[] spawnPoints = teamSpawnTransformsDict[team].transforms;
			RevivePlayer (id, spawnPoints[Random.Range(0, spawnPoints.Length)]);
		}

		public void OnGoaled (SoccerGoal goal)
		{
			Match.Team goalingTeam;
			Match.Team victimTeam = goal.owner;
			if (goal.owner == playerTeamsDict.Values.Get(0))
				goalingTeam = playerTeamsDict.Values.Get(1);
			else
				goalingTeam = playerTeamsDict.Values.Get(0);
			goalingTeam.score ++;
			Scoreboard.instance.UpdateEntry (goalingTeam);
			if (ranked && localPlayer.owner == goalingTeam)
			{
				LocalUserInfo.goalsMade ++;
				LocalUserInfo.goalsMadeInCurrentSeason ++;
			}
			else if (localPlayer.owner == victimTeam)
			{
				LocalUserInfo.goaledOn ++;
				LocalUserInfo.goaledOnInCurrentSeason ++;
			}
			Transform[] teamSpawnTransforms = teamSpawnTransformsDict[goalingTeam].transforms;
			for (int i = 0; i < goalingTeam.representatives.Length; i ++)
			{
				Player player = goalingTeam.representatives[i];
				player.trs.position = teamSpawnTransforms[i % teamSpawnTransforms.Length].position;
			}
			teamSpawnTransforms = teamSpawnTransformsDict[victimTeam].transforms;
			for (int i = 0; i < victimTeam.representatives.Length; i ++)
			{
				Player player = victimTeam.representatives[i];
				player.trs.position = teamSpawnTransforms[i % teamSpawnTransforms.Length].position;
			}
			Soccerball.instance.trs.position = ballSpawnPosition;
			GameManager.updatables = GameManager.updatables.Add(new CountdownUpdater());
			if (!hasEnded && goalingTeam.score >= maxScore)
			{
				if (ranked)
				{
					if (localPlayer.owner == goalingTeam)
					{
						float skillChange = 1f - ((float) victimTeam.score / maxScore);
						LocalUserInfo.skillDict[name] += skillChange;
						LocalUserInfo.skillInCurrentSeasonDict[name] += skillChange;
						LocalUserInfo.winsDict[name] ++;
						LocalUserInfo.winsInCurrentSeasonDict[name] ++;
					}
					else
					{
						float skillChange = 1f - ((float) localPlayer.owner.score / maxScore);
						LocalUserInfo.skillDict[name] -= skillChange;
						LocalUserInfo.skillInCurrentSeasonDict[name] -= skillChange;
						LocalUserInfo.lossesDict[name] ++;
						LocalUserInfo.lossesInCurrentSeasonDict[name] ++;
					}
				}
				End ();
			}
		}

		[Serializable]
		public class SpawnPointGroup
		{
			public Transform[] transforms = new Transform[0];
		}

		class CountdownUpdater : IUpdatable
		{
			float timer;

			public CountdownUpdater ()
			{
				instance.countdownBar.parent.gameObject.SetActive(true);
			}

			public void DoUpdate ()
			{
				timer += Time.deltaTime;
				instance.countdownBar.localScale = instance.countdownBar.localScale.SetX(instance.countdownDuration - timer / instance.countdownDuration);
				if (timer < instance.countdownDuration / 3)
					instance.countdownText.Text = "Ready...";
				else if (timer < instance.countdownDuration / 3 * 2)
					instance.countdownText.Text = "Set...";
				else
				{
					instance.countdownText.Text = "Go!";
					if (timer >= instance.countdownDuration)
					{
						instance.countdownBar.parent.gameObject.SetActive(false);
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
			}
		}
	}
}