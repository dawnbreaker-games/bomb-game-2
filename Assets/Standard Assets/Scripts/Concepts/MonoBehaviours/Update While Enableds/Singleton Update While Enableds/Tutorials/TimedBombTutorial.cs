using UnityEngine;
using UnityEngine.InputSystem;

namespace BombGame
{
	public class TimedBombTutorial : Tutorial
	{
		public GameObject activateOnUnequipTimedBomb;
		public GameObject activateOnEquipTimedBomb;
		public GameObject deactivateOnEquipTimedBomb;
		public GameObject activateOnArmTimedBomb;
		public GameObject activateOnThrowTimedBomb;
		public PlayerRecording playerRecording;
		Bomb armedTimedBomb;
		bool equipedTimedBomb;
		bool didArmTimedBomb;

		public override void DoUpdate ()
		{
			if (activateOnThrowTimedBomb.activeSelf)
			{
				if (Keyboard.current.hKey.wasPressedThisFrame)
				{
					playerRecording.enabled = false;
					playerRecording.currentKeyframeIndex = 0;
					playerRecording.timer = 0;
					playerRecording.enabled = true;
				}
			}
			else
			{
				if (Player.instance.currentBomb.explodeOnHit)
				{
					if (equipedTimedBomb)
					{
						GameManager.ActivateGameObjectForever (activateOnUnequipTimedBomb);
						GameManager.DeactivateGameObjectForever (activateOnEquipTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
					}
				}
				else
				{
					if (Player.instance.armedBombNamesDict.Count > 0)
					{
						armedTimedBomb = Player.instance.GetComponentInChildren<Bomb>();
						if (!didArmTimedBomb)
						{
							GameManager.ActivateGameObjectForever (activateOnArmTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
						}
						didArmTimedBomb = true;
					}
					else if (armedTimedBomb != null && armedTimedBomb.canExplode)
					{
						if (!activateOnThrowTimedBomb.activeSelf)
						{
							GameManager.DeactivateGameObjectForever (activateOnArmTimedBomb);
							GameManager.ActivateGameObjectForever (activateOnThrowTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
						}
					}
					else if (!didArmTimedBomb)
					{
						GameManager.DeactivateGameObjectForever (activateOnUnequipTimedBomb);
						GameManager.ActivateGameObjectForever (activateOnEquipTimedBomb);
						GameManager.DeactivateGameObjectForever (deactivateOnEquipTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
						equipedTimedBomb = true;
					}
				}
			}
		}
	}
}