﻿using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BombGame
{
	[ExecuteInEditMode]
	public class WorldMap : SingletonUpdateWhileEnabled<WorldMap>
	{
		public Tilemap unexploredTilemap;
		public TileBase unexploredTile;
		public float cameraMoveSpeed;
		public float normalizedScreenBorder;
		public static WorldMapIcon[] worldMapIcons = new WorldMapIcon[0];
		public static bool isOpen;
		public static SavePoint savePointPlayerIsAt;
		public static bool foundNewCellSinceSaved;
		public static bool[] exploredCellPositions = new bool[0];
		public static bool[] ExploredCellPositions
		{
			get
			{
				return SaveAndLoadManager.GetBoolArray("Explored cells", new bool[0]);
			}
			set
			{
				SaveAndLoadManager.SetBoolArray ("Explored cells", value);
			}
		}
		HashSet<Vector2Int> exploredCellPositionsAtLastTimeOpened = new HashSet<Vector2Int>();
		Vector2Int minCellPosition;
		Vector2Int maxCellPosition;
		Vector2Int previousMinCellPosition;
		bool canControlCamera;
		Vector2 moveInput;
		Rect screenWithoutBorder;
		SavePoint fastTravelToSavePoint;

#if UNITY_EDITOR
		public void UpdateUnexploredTilemap ()
		{
			Tilemap tilemap = World.instance.tilemaps[0];
			tilemap.CompressBounds();
			BoundsInt cellBounds = tilemap.cellBounds;
			for (int i = 1; i < World.instance.tilemaps.Length; i ++)
			{
				tilemap = World.instance.tilemaps[i];
				tilemap.CompressBounds();
				cellBounds.SetMinMax(Vector3Int.Min(tilemap.cellBounds.min, cellBounds.min), Vector3Int.Max(tilemap.cellBounds.max, cellBounds.max));
			}
			unexploredTilemap.ClearAllTiles();
			TileBase[] tiles = new TileBase[cellBounds.size.x * cellBounds.size.y];
			for (int i = 0; i < tiles.Length; i ++)
				tiles[i] = unexploredTile;
			unexploredTilemap.SetTilesBlock(cellBounds, tiles);
		}
#endif

#if !UNITY_WEBGL
		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			SaveAndLoadManager.Init ();
			GameCamera.Instance.Awake ();
			minCellPosition = (unexploredTilemap.WorldToCell(GameCamera.instance.viewRect.min) - unexploredTilemap.cellBounds.min).ToVec2Int();
			// minCellPosition.Clamp(Vector2Int.zero, unexploredTilemap.cellBounds.size.ToVec2Int() - Vector2Int.one);
			maxCellPosition = (unexploredTilemap.WorldToCell(GameCamera.instance.viewRect.max) - unexploredTilemap.cellBounds.min).ToVec2Int();
			maxCellPosition.Clamp(Vector2Int.zero, unexploredTilemap.cellBounds.size.ToVec2Int() - Vector2Int.one);
			exploredCellPositions = new bool[unexploredTilemap.cellBounds.size.x * unexploredTilemap.cellBounds.size.y];
			if (ExploredCellPositions.Length == 0)
			{
				for (int x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					for (int y = minCellPosition.y; y <= maxCellPosition.y; y ++)
						exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
				}
			}
			else
				ExploredCellPositions.CopyTo(exploredCellPositions, 0);
			previousMinCellPosition = minCellPosition;
			screenWithoutBorder = new Rect();
			screenWithoutBorder.size = new Vector2(Screen.width - normalizedScreenBorder * Screen.width, Screen.height - normalizedScreenBorder * Screen.height);
			screenWithoutBorder.center = new Vector2(Screen.width / 2, Screen.height / 2);
			WorldMapCamera.Instance.HandleViewSize ();
		}
		
		public override void DoUpdate ()
		{
			if (!isOpen)
			{
				UpdateExplored ();
				if (Keyboard.current.tabKey.wasPressedThisFrame)
					Open ();
			}
			else
			{
				WorldMapCamera.instance.DoUpdate ();
				moveInput = Vector2.zero;
				// GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(true);
				// if (InputManager.UsingGamepad)
				// {
				// 	GameManager.activeCursorEntry.rectTrs.position += (Vector3) moveInput * GameManager.cursorMoveSpeed * Time.unscaledDeltaTime;
				// 	GameManager.activeCursorEntry.rectTrs.position = GameManager.activeCursorEntry.rectTrs.position.ClampComponents(Vector3.zero, new Vector2(Screen.width, Screen.height));
				// }
				if (!screenWithoutBorder.Contains(Mouse.current.position.ReadValue()))
				{
					moveInput = Mouse.current.position.ReadValue() - new Vector2(Screen.width / 2, Screen.height / 2);
					moveInput /= new Vector2(Screen.width / 2, Screen.height / 2).magnitude;
					if (canControlCamera)
						WorldMapCamera.instance.trs.position += (Vector3) moveInput * cameraMoveSpeed * Time.unscaledDeltaTime;
					// if (GameManager.activeCursorEntry.name != "Arrow")
					// {
					// 	GameManager.cursorEntriesDict["Arrow"].SetAsActive ();
					// 	GameManager.activeCursorEntry.rectTrs.position = GameManager.cursorEntriesDict["Default"].rectTrs.position;
					// }
					// GameManager.activeCursorEntry.rectTrs.up = moveInput;
					// if (GameManager.Instance.worldMapTutorialConversation.currentDialog == GameManager.Instance.worldMapMoveViewTutorialDialog)
					// 	DialogManager.Instance.EndDialog (GameManager.Instance.worldMapMoveViewTutorialDialog);
				}
				// else if (GameManager.activeCursorEntry.name != "Default")
				// {
				// 	GameManager.cursorEntriesDict["Default"].SetAsActive ();
				// 	GameManager.activeCursorEntry.rectTrs.position = GameManager.cursorEntriesDict["Arrow"].rectTrs.position;
				// }
				if (savePointPlayerIsAt != null)
					HandleFastTravel ();
				if (Keyboard.current.tabKey.wasPressedThisFrame)
					Close ();
			}
		}

		void HandleFastTravel ()
		{
			for (int i = 0; i < SavePoint.instances.Count; i ++)
			{
				SavePoint savePoint = SavePoint.instances[i];
				if (savePoint.Found)
				{
					if (savePoint != savePointPlayerIsAt && savePoint.worldMapIcon.iconCollider.bounds.ToRect().Contains(WorldMapCamera.instance.camera.ScreenToWorldPoint(Mouse.current.position.ReadValue())))
					{
						if (fastTravelToSavePoint != null)
							fastTravelToSavePoint.worldMapIcon.Unhighlight ();
						fastTravelToSavePoint = savePoint;
						fastTravelToSavePoint.worldMapIcon.Highlight ();
						break;
					}
				}
			}
			if (fastTravelToSavePoint != null && Mouse.current.leftButton.wasPressedThisFrame)
			{
				Player.instance.SavedPosition = fastTravelToSavePoint.worldMapIcon.objectCollider.bounds.center;
				Close ();
				isOpen = false;
				_SceneManager.Instance.RestartScene ();
			}
		}

		IEnumerator OpenRoutine ()
		{
			canControlCamera = false;
			foreach (WorldMapIcon worldMapIcon in worldMapIcons)
			{
				foreach (Vector2Int position in worldMapIcon.cellBoundsRect.allPositionsWithin)
				{
					Vector2Int _position = position - (Vector2Int) unexploredTilemap.cellBounds.min;
					if (!worldMapIcon.onlyMakeIfExplored || exploredCellPositions[_position.x + _position.y * unexploredTilemap.cellBounds.size.x])
						worldMapIcon.MakeIcon ();
				}
			}
			HashSet<Vector3Int> exploredCellPositionsSinceLastTimeOpened = new HashSet<Vector3Int>();
			for (int i = 0; i < exploredCellPositions.Length; i ++)
			{
				bool exploredCellPosition = exploredCellPositions[i];
				if (exploredCellPosition)
					exploredCellPositionsSinceLastTimeOpened.Add(new Vector3Int(i % unexploredTilemap.cellBounds.size.x, i / unexploredTilemap.cellBounds.size.x) + unexploredTilemap.cellBounds.min);
			}
			foreach (Vector2Int exploredCellPositionAtLastTimeOpened in exploredCellPositionsAtLastTimeOpened)
				exploredCellPositionsSinceLastTimeOpened.Remove(exploredCellPositionAtLastTimeOpened.ToVec3Int());
			Vector3Int[] _exploredCellPositionsSinceLastTimeOpened = new Vector3Int[exploredCellPositionsSinceLastTimeOpened.Count];
			exploredCellPositionsSinceLastTimeOpened.CopyTo(_exploredCellPositionsSinceLastTimeOpened);
			unexploredTilemap.SetTiles(_exploredCellPositionsSinceLastTimeOpened, new TileBase[exploredCellPositionsSinceLastTimeOpened.Count]);
			WorldMapCamera.instance.trs.position = Player.Instance.trs.position.SetZ(WorldMapCamera.instance.trs.position.z);
			WorldMapCamera.instance.gameObject.SetActive(true);
			// if (InputManager.UsingGamepad)
			// {
			// 	GameManager.cursorEntriesDict["Default"].SetAsActive ();
			// 	GameManager.activeCursorEntry.rectTrs.localPosition = Vector2.zero;
			// }
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			canControlCamera = true;
		}

		public virtual void Open ()
		{
			isOpen = true;
			GameManager.SetPaused (true);
			unexploredTilemap.gameObject.SetActive(true);
			foreach (WorldPiece worldPiece in World.instance.piecesDict.Values)
				worldPiece.gameObject.SetActive(true);
			StopAllCoroutines();
			StartCoroutine(OpenRoutine ());
		}

		public virtual void Close ()
		{
			isOpen = false;
			GameManager.SetPaused (false);
			for (int i = 0; i < exploredCellPositions.Length; i ++)
			{
				bool exploredCellPosition = exploredCellPositions[i];
				if (exploredCellPosition)
					exploredCellPositionsAtLastTimeOpened.Add(new Vector2Int(i % unexploredTilemap.cellBounds.size.x, i / unexploredTilemap.cellBounds.size.x) + unexploredTilemap.cellBounds.min.ToVec2Int());
			}
			foreach (WorldMapIcon worldMapIcon in worldMapIcons)
			{
				worldMapIcon.DestroyIcon ();
				worldMapIcon.Unhighlight ();
			}
			fastTravelToSavePoint = null;
			WorldMapCamera.instance.gameObject.SetActive(false);
			// if (!InputManager.UsingGamepad)
			// 	GameManager.cursorEntriesDict["Default"].SetAsActive ();
			// else
			// 	GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(false);
			unexploredTilemap.gameObject.SetActive(false);
			foreach (WorldPiece worldPiece in World.instance.piecesDict.Values)
				worldPiece.gameObject.SetActive(World.instance.activePieces.Contains(worldPiece));
		}
		
		void UpdateExplored ()
		{
			int x;
			int y;
			minCellPosition = (unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.min) - unexploredTilemap.cellBounds.min).ToVec2Int();
			minCellPosition.Clamp(Vector2Int.zero, unexploredTilemap.cellBounds.size.ToVec2Int() - Vector2Int.one);
			maxCellPosition = (unexploredTilemap.WorldToCell(GameCamera.instance.viewRect.max) - unexploredTilemap.cellBounds.min).ToVec2Int();
			maxCellPosition.Clamp(Vector2Int.zero, unexploredTilemap.cellBounds.size.ToVec2Int() - Vector2Int.one);
			if (minCellPosition.x > previousMinCellPosition.x)
			{
				x = maxCellPosition.x;
				for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
				{
					if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
					{
						exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
						foundNewCellSinceSaved = true;
					}
				}
				if (minCellPosition.y > previousMinCellPosition.y)
				{
					y = maxCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
				else if (minCellPosition.y < previousMinCellPosition.y)
				{
					y = minCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
			}
			else if (minCellPosition.x < previousMinCellPosition.x)
			{
				x = minCellPosition.x;
				for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
				{
					if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
					{
						exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
						foundNewCellSinceSaved = true;
					}
				}
				if (minCellPosition.y > previousMinCellPosition.y)
				{
					y = maxCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
				else if (minCellPosition.y < previousMinCellPosition.y)
				{
					y = minCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
			}
			else if (minCellPosition.y > previousMinCellPosition.y)
			{
				y = maxCellPosition.y;
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
					{
						exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
						foundNewCellSinceSaved = true;
					}
				}
				if (minCellPosition.x > previousMinCellPosition.x)
				{
					x = maxCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
				else if (minCellPosition.x < previousMinCellPosition.x)
				{
					x = minCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
			}
			else if (minCellPosition.y < previousMinCellPosition.y)
			{
				y = minCellPosition.y;
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
					{
						exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
						foundNewCellSinceSaved = true;
					}
				}
				if (minCellPosition.x > previousMinCellPosition.x)
				{
					x = maxCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
				else if (minCellPosition.x < previousMinCellPosition.x)
				{
					x = minCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						if (!exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x])
						{
							exploredCellPositions[x + y * unexploredTilemap.cellBounds.size.x] = true;
							foundNewCellSinceSaved = true;
						}
					}
				}
			}
			previousMinCellPosition = minCellPosition;
		}
#endif
	}
}