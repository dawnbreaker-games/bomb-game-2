﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class Patrol : MonoBehaviour, IUpdatable
	{
		public bool isFlying;
		public float patrolRange;
		public float stopRange;
		public LayerMask whatIPatrolOn;
		public Transform trs;
		public CharacterController controller;
		public Rigidbody rigid;
		public float moveSpeed;
		float yVel;
		Vector3 move;
		Vector3 initPosition;
		Vector3 destination;
		float stopRangeSqr;
		Vector3 toDestination;

		void Awake ()
		{
			initPosition = trs.position;
			stopRangeSqr = stopRange * stopRange;
		}

		void OnEnable ()
		{
			SetDestination ();
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			toDestination = destination - trs.position;
			if (!isFlying)
				toDestination = toDestination.SetY(0);
			move = Vector3.ClampMagnitude(toDestination, 1);
			move *= moveSpeed;
			trs.forward = move;
			HandleGravity ();
			if (controller != null && controller.enabled)
				controller.Move(move * Time.deltaTime);
			else
				rigid.linearVelocity = move;
			if (toDestination.sqrMagnitude <= stopRangeSqr || (controller != null && controller.collisionFlags.ToString().Contains("Sides")))
				SetDestination ();
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		void SetDestination ()
		{
			if (isFlying)
			{
				destination = initPosition + Random.onUnitSphere * Random.value * patrolRange;
				return;
			}
			do
			{
				destination = initPosition + (Random.insideUnitCircle * patrolRange).XYToXZ();
				RaycastHit hit;
				if (Physics.Raycast(destination.SetY(trs.position.y + patrolRange), Vector3.down, out hit, Mathf.Infinity, whatIPatrolOn))
					return;
			} while (true);
		}

		void HandleGravity ()
		{
			if (controller != null && controller.enabled && !controller.isGrounded)
			{
				yVel += Physics.gravity.y * Time.deltaTime;
				move += Vector3.up * yVel;
			}
			else
				yVel = 0;
		}
	}
}