using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace BombGame
{
	public class Enemy_Sandworm : Enemy
	{
		public Collider2D[] groundColliders = new Collider2D[0];
		public Transform tailPolygonCollidersParent;
		public PolygonCollider2D[] tailPolygonColliders = new PolygonCollider2D[0];
		public Collider2D playerHazardCollider;
		public float damage;
		public float turnRate;
		public float dartSpeed;
		public float nonDartingSpeed;
		public float dartAngle;
		public float minAngleToNotMove;
		public float accelerationRate;
		public float deccelerationRate;
		public LayerMask whatIsGround;
		public LineRenderer tailLineRenderer;
		public float minTailPointSeparation;
		public float targetTailLength;
		[HideInInspector]
		public Vector2 extraVelocity;
		public float extraVelocitySlowDownSpeed;
		List<Vector2> previousPositions2D = new List<Vector2>();
		List<Vector3> previousPositions3D = new List<Vector3>();
		List<TailPoint> tailPoints = new List<TailPoint>();
		float tailLength;
		Vector2 previousPosition;
		float moveSpeed;
		const float MIN_ANGLE_DIFF = .1f;

		void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			Vector2 tailPosition = trs.position - trs.up * targetTailLength;
			previousPosition = tailPosition;
			for (float length = 0; length < targetTailLength; length += minTailPointSeparation)
			{
				MakeTailPoint (tailPosition);
				tailPosition += (Vector2) trs.up * minTailPointSeparation;
			}
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			moveSpeed = rigid.velocity.magnitude;
		}

		public override void DoUpdate ()
		{
			bool isGrounded = false;
			for (int i = 0; i < groundColliders.Length; i ++)
			{
				Collider2D groundCollider = groundColliders[i];
				if (groundCollider.IsTouchingLayers(whatIsGround))
				{
					isGrounded = true;
					break;
				}
			}
			if (isGrounded)
			{
				rigid.gravityScale = 0;
				trs.up = trs.up.RotateTo(Player.instance.trs.position - trs.position, turnRate * Time.deltaTime);
				HandleMovement ();
			}
			else
			{
				rigid.gravityScale = 1;
				trs.up = rigid.velocity;
				moveSpeed = rigid.velocity.magnitude;
			}
			MakeTailPoint (trs.position);
		}

		void HandleMovement ()
		{
			float targetMoveSpeed;
			if (Vector2.Angle(trs.up.RotateTo(Player.instance.trs.position - trs.position, minAngleToNotMove), (Vector2) (Player.instance.trs.position - trs.position)) >= MIN_ANGLE_DIFF)
				targetMoveSpeed = 0;
			else if (Vector2.Angle(trs.up.RotateTo(Player.instance.trs.position - trs.position, dartAngle), (Vector2) (Player.instance.trs.position - trs.position)) < MIN_ANGLE_DIFF)
				targetMoveSpeed = dartSpeed;
			else
				targetMoveSpeed = nonDartingSpeed;
			if (targetMoveSpeed > moveSpeed)
				moveSpeed = Mathf.LerpUnclamped(moveSpeed, targetMoveSpeed, accelerationRate * Time.deltaTime);
			else
				moveSpeed = Mathf.LerpUnclamped(moveSpeed, targetMoveSpeed, deccelerationRate * Time.deltaTime);
			rigid.velocity = (Vector2) trs.up * moveSpeed + extraVelocity;
			float extraVelocitySlowDownAmount = extraVelocitySlowDownSpeed * Time.deltaTime;
			if (extraVelocity.sqrMagnitude > extraVelocitySlowDownAmount * extraVelocitySlowDownAmount)
				extraVelocity -= extraVelocity.normalized * extraVelocitySlowDownAmount;
			else
				extraVelocity = Vector2.zero;
		}

		public override void OnTriggerEnter2D (Collider2D other)
		{
			base.OnTriggerEnter2D (other);
			if (playerHazardCollider.IsTouching(Player.instance.polygonCollider))
				Player.instance.TakeDamage (damage, null);
		}

		void MakeTailPoint (Vector2 position)
		{
			Vector2 move = position - previousPosition;
			float distanceToPreviousPosition = move.magnitude;
			float totalMoveAmount = distanceToPreviousPosition;
			while (totalMoveAmount > 0)
			{
				float moveAmount = Mathf.Min(minTailPointSeparation, totalMoveAmount);
				previousPosition += move.normalized * moveAmount;
				previousPositions2D.Add(previousPosition);
				previousPositions3D.Add(previousPosition);
				tailPoints.Add(new TailPoint(previousPosition, moveAmount));
				tailLength += moveAmount;
				totalMoveAmount -= moveAmount;
			}
			if (tailPoints.Count > 1)
			{
				totalMoveAmount = tailLength - targetTailLength;
				while (totalMoveAmount > 0)
				{
					TailPoint tailPoint = tailPoints[0];
					tailPoints.RemoveAt(0);
					previousPositions2D.RemoveAt(0);
					previousPositions3D.RemoveAt(0);
					tailLength -= tailPoint.distanceToPreviousPoint;
					if (tailPoints.Count == 0)
						break;
					totalMoveAmount -= tailPoint.distanceToPreviousPoint;
				}
				previousPosition = previousPositions2D[0];
				move = (previousPosition - previousPositions2D[1]).normalized;
				totalMoveAmount = targetTailLength - tailLength;
				while (totalMoveAmount > 0)
				{
					float moveAmount = Mathf.Min(minTailPointSeparation, totalMoveAmount);
					previousPosition += move.normalized * moveAmount;
					previousPositions2D.Insert(0, previousPosition);
					previousPositions3D.Insert(0, previousPosition);
					tailPoints.Insert(0, new TailPoint(previousPosition, moveAmount));
					tailLength += moveAmount;
					totalMoveAmount -= moveAmount;
				}
			}
			previousPosition = position;
			tailLineRenderer.positionCount = previousPositions3D.Count;
			tailLineRenderer.SetPositions(previousPositions3D.ToArray());
			if (previousPositions2D.Count > 1)
			{
				tailPolygonCollidersParent.position = Vector3.zero;
				tailPolygonCollidersParent.up = Vector3.up;
				for (int i = 0; i < tailPolygonColliders.Length; i ++)
				{
					PolygonCollider2D tailPolygonCollider = tailPolygonColliders[i];
					tailPolygonCollider.points = Stroke2D.FromPoints(tailLineRenderer.widthCurve, previousPositions2D.ToArray()).corners;
				}
			}
		}

		struct TailPoint
		{
			public Vector2 position;
			public float distanceToPreviousPoint;

			public TailPoint (Vector2 position, float distanceToPreviousPoint)
			{
				this.position = position;
				this.distanceToPreviousPoint = distanceToPreviousPoint;
			}
		}
	}
}