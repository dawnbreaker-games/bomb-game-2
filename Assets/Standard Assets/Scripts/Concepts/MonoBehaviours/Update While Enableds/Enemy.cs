using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	[ExecuteInEditMode]
	public class Enemy : UpdateWhileEnabled, IDestructable
	{
		public uint maxHp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public uint MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public BulletPatternEntry bulletPatternEntry;
		public Animator animator;
		public float visionRange;
		public Transform visionRangeTrs;
		public EnemyGroup enemyGroup;
		public Transform trs;
		[HideInInspector]
		public Vector2 initPosition;
		[HideInInspector]
		public float initRotation;
		public Rigidbody2D rigid;
		public static Enemy[] instances = new Enemy[0];
		float hp;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (rigid == null)
				rigid = GetComponent<Rigidbody2D>();
			if (animator == null)
				animator = GetComponent<Animator>();
			visionRangeTrs.SetWorldScale (Vector3.one * visionRange * 2);
			initPosition = trs.position;
			initRotation = trs.eulerAngles.z;
		}
#endif

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			hp = maxHp;
		}

		public override void DoUpdate ()
		{
			trs.up = Player.instance.trs.position - trs.position;
		}
		
		public void TakeDamage (float amount, Player attacker)
		{
			float previousHp = hp;
			hp -= amount;
			if (hp <= 0 && previousHp > 0)
				Death (attacker);
		}
		
		public void Death (Player killer)
		{
			// Destroy(gameObject);
			gameObject.SetActive(false);
		}

		public void Shoot ()
		{
			bulletPatternEntry.Shoot ();
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			if (other != Player.instance.polygonCollider)
				return;
			OnTriggerStay2D (other);
			if (enemyGroup != null)
				enemyGroup.visionRangesPlayerIsIn ++;
		}

		void OnTriggerStay2D (Collider2D other)
		{
			if (other != Player.instance.polygonCollider)
				return;
			enabled = true;
			if (enemyGroup == null)
				animator.Play("Attack");
			else
			{
				for (int i = 0; i < enemyGroup.enemies.Length; i ++)
				{
					Enemy enemy = enemyGroup.enemies[i];
					if (enemy != null)
					{
						enemy.animator.Play("Attack");
						enemy.enabled = true;
					}
				}
			}
		}

		void OnTriggerExit2D (Collider2D other)
		{
			if (other != Player.instance.polygonCollider)
				return;
			if (enemyGroup == null)
			{
				animator.Play("Idle");
				enabled = false;
			}
			else
			{
				enemyGroup.visionRangesPlayerIsIn --;
				if (enemyGroup.visionRangesPlayerIsIn == 0)
				{
					for (int i = 0; i < enemyGroup.enemies.Length; i ++)
					{
						Enemy enemy = enemyGroup.enemies[i];
						if (enemy != null)
						{
							enemy.animator.Play("Idle");
							enemy.enabled = false;
						}
					}
				}
			}
		}
	}
}