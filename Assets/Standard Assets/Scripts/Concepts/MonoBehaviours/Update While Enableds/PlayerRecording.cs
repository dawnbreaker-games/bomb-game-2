using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace BombGame
{
	public class PlayerRecording : UpdateWhileEnabled
	{
		public Keyframe[] keyframes = new Keyframe[0];
		[HideInInspector]
		public int currentKeyframeIndex;
		public Bomb[] bombPrefabs = new Bomb[0];
		[HideInInspector]
		public float timer;
#if UNITY_EDITOR
		public bool record;
#endif
		[SerializeField]
		public static float previousPlayerReloadTimer;
		[SerializeField]
		public static int previousPlayerArmedBombsCount;
		public static List<BombUpdater> bombUpdaters = new List<BombUpdater>();
		Keyframe currentKeyframe;
		Keyframe nextKeyframe;
		Player playerGhost;
		Vector2 previousPlayerGhostPosition;
#if UNITY_EDITOR
		List<Keyframe> _keyframes = new List<Keyframe>();
		bool previousRecord;
		float previousTime;
		float lastKeyframeTime;

		void OnValidate ()
		{
			if (record)
			{
				if (!previousRecord)
				{
					EditorApplication.isPlaying = true;
					if (keyframes.Length > 0)
					{
						Keyframe lastKeyframe = keyframes[keyframes.Length - 1];
						lastKeyframeTime = lastKeyframe.time;
						previousTime = lastKeyframeTime;
						Player.Instance.trs.position = lastKeyframe.position;
						Player.instance.trs.localScale = Player.instance.trs.localScale.SetX(lastKeyframe.facingRight.PositiveOrNegative());
						Player.instance.reloadTimer = previousPlayerReloadTimer;
						// TODO: Set the player's armed bombs
					}
					else
					{
						previousTime = Time.time;
						previousPlayerReloadTimer = Player.Instance.reloadTimer;
						previousPlayerArmedBombsCount = Player.instance.armedBombNamesDict.Count;
					}
					EditorApplication.update += Record;
				}
			}
			else if (previousRecord)
			{
				EditorApplication.update -= Record;
				keyframes = _keyframes.ToArray();
				_keyframes.Clear();
				lastKeyframeTime = 0;
			}
			previousRecord = record;
		}

		void Record ()
		{
			if (Time.frameCount <= GameManager.LAGGY_FRAMES_ON_LOAD_SCENE)
			{
				previousTime = Time.time;
				return;
			}
			float time = Time.time;
			lastKeyframeTime += time - previousTime;
			_keyframes.Add(new Keyframe(Player.Instance, lastKeyframeTime));
			previousTime = time;
			previousPlayerReloadTimer = Player.instance.reloadTimer;
			previousPlayerArmedBombsCount = Player.instance.armedBombNamesDict.Count;
		}
#endif

		public override void OnEnable ()
		{
			Player player = Player.Instance;
			playerGhost = Instantiate(player);
			playerGhost.enabled = false;
			playerGhost.rigid.simulated = false;
			Destroy(playerGhost.GetComponent<AffectedByRift>());
			playerGhost.renderer.material.SetColor("_tint", playerGhost.renderer.material.GetColor("_tint").DivideAlpha(2));
			playerGhost.aimingVisualizer.startColor = playerGhost.aimingVisualizer.startColor.DivideAlpha(2);
			playerGhost.aimingVisualizer.endColor = playerGhost.aimingVisualizer.endColor.DivideAlpha(2);
			Destroy(playerGhost.GetComponentInChildren<GameCamera>().gameObject);
			Player.instance = player;
			previousPlayerGhostPosition = playerGhost.trs.position;
			nextKeyframe = keyframes[currentKeyframeIndex + 1];
			GoToKeyframe (keyframes[currentKeyframeIndex]);
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			timer += Time.deltaTime;
			while (timer >= nextKeyframe.time)
			{
				currentKeyframeIndex ++;
				if (currentKeyframeIndex == keyframes.Length)
				{
					currentKeyframeIndex = 0;
					enabled = false;
					return;
				}
				else
				{
					if (currentKeyframeIndex < keyframes.Length - 1)
					{
						nextKeyframe = keyframes[currentKeyframeIndex + 1];
						previousPlayerGhostPosition	= playerGhost.trs.position;
					}
					GoToKeyframe (keyframes[currentKeyframeIndex]);
				}
			}
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			Destroy(playerGhost.gameObject);
			for (int i = 0; i < bombUpdaters.Count; i ++)
			{
				BombUpdater bombUpdater = bombUpdaters[i];
				GameManager.updatables = GameManager.updatables.Remove(bombUpdater);
				Destroy(bombUpdater.bomb.gameObject);
			}
			bombUpdaters.Clear();
		}

		void GoToKeyframe (Keyframe keyframe)
		{
			currentKeyframe = keyframe;
			float timeBetweenKeyframes = nextKeyframe.time - currentKeyframe.time;
			float timeToNextKeyframe = nextKeyframe.time - timer;
			float timeToNextKeyframeFraction = timeToNextKeyframe / timeBetweenKeyframes;
			playerGhost.trs.position = Vector2.Lerp(currentKeyframe.position, nextKeyframe.position, timeToNextKeyframeFraction);
			playerGhost.trs.localScale = playerGhost.trs.localScale.SetX(nextKeyframe.facingRight.PositiveOrNegative());
			playerGhost.aimVector = Vector2.Lerp(currentKeyframe.aimVector, nextKeyframe.aimVector, timeToNextKeyframeFraction);
			Vector2 currentShootPosition = playerGhost.shootSpawnTrs.position;
			Vector2 playerGhostVelocity = ((Vector2) playerGhost.trs.position - previousPlayerGhostPosition) * Time.deltaTime;
			Vector2 currentShootVelocity = playerGhost.aimVector * playerGhost.shootSpeed + playerGhostVelocity;
			List<Vector3> positions = new List<Vector3>();
			positions.Add(currentShootPosition);
			while (GameCamera.instance.viewRect.Contains(currentShootPosition))
			{
				currentShootVelocity += Physics2D.gravity * Time.fixedDeltaTime;
				currentShootVelocity *= 1f - Time.fixedDeltaTime * playerGhost.currentBomb.rigid.drag;
				currentShootPosition += currentShootVelocity * Time.fixedDeltaTime;
				RaycastHit2D hit = Physics2DExtensions.LinecastWithWidth(positions[positions.Count - 1], currentShootPosition, playerGhost.aimingVisualizer.startWidth, playerGhost.whatBombsCollideWith);
				if (hit.collider != null)
				{
					positions.Add(hit.point);
					break;
				}
				if (positions.Contains(currentShootPosition))
					break;
				positions.Add(currentShootPosition);
			}
			playerGhost.aimingVisualizer.positionCount = positions.Count;
			playerGhost.aimingVisualizer.SetPositions(positions.ToArray());
			if (currentKeyframe.thrownOrArmedBombIndex != -1)
			{
				Bomb bombPrefab = bombPrefabs[currentKeyframe.thrownOrArmedBombIndex];
				playerGhost.currentBomb = bombPrefab;
				Bomb bomb = playerGhost.Shoot(playerGhost.aimVector * playerGhost.shootSpeed + playerGhostVelocity);
				Destroy(bomb.GetComponent<AffectedByRift>());
				Destroy(bomb.GetComponent<PolygonCollider2D>());
				Destroy(bomb.explosion.GetComponent<CircleCollider2D>());
				Destroy(bomb.explosion.GetComponent<PolygonCollider2D>());
				bomb.spriteRenderer.color = bomb.spriteRenderer.color.DivideAlpha(2); 
				bomb.explosion.killZoneSpriteRenderer.color = bomb.explosion.killZoneSpriteRenderer.color.DivideAlpha(2);
				bomb.explosion.windZoneSpriteRenderer.color = bomb.explosion.windZoneSpriteRenderer.color.DivideAlpha(2);
				if (bomb.explodeOnHit)
				{
					Destroy(bomb.collider);
					BombUpdater bombUpdater = new BombUpdater(bomb);
					bombUpdaters.Add(bombUpdater);
					GameManager.updatables = GameManager.updatables.Add(bombUpdater);
				}
			}
		}

		[Serializable]
		public struct Keyframe
		{
			public float time;
			public Vector2 position;
			public bool facingRight;
			public Vector2 aimVector;
			public int thrownOrArmedBombIndex;

			public Keyframe (float time, Vector2 position, bool facingRight, Vector2 aimVector, int thrownOrArmedBombIndex)
			{
				this.time = time;
				this.position = position;
				this.facingRight = facingRight;
				this.aimVector = aimVector;
				this.thrownOrArmedBombIndex = thrownOrArmedBombIndex;
			}

			public Keyframe (Player player, float time) : this (time, player.trs.position, player.trs.lossyScale.x > 0, player.aimVector, GetThrownOrArmedBombIndex(player))
			{
			}

			static int GetThrownOrArmedBombIndex (Player player)
			{
				if (player.reloadTimer < previousPlayerReloadTimer || player.armedBombNamesDict.Count > previousPlayerArmedBombsCount)
					return player.currentBombIndex;
				else
					return -1;
			}
		}

		public class BombUpdater : IUpdatable
		{
			public Bomb bomb;

			public BombUpdater (Bomb bomb)
			{
				this.bomb = bomb;
			}

			public void DoUpdate ()
			{
				if (Physics2D.OverlapCircle(bomb.trs.position, bomb.radius, Player.instance.whatBombsCollideWith))
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					bombUpdaters.Remove(this);
					bomb.Explode ();
				}
			}
		}
	}
}