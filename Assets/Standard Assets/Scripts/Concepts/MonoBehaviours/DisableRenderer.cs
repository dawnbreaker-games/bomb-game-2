using UnityEngine;

public class DisableRenderer : MonoBehaviour
{
	public Renderer renderer;

	void OnValidate ()
	{
		if (renderer == null)
			renderer = GetComponent<Renderer>();
	}

	void Awake ()
	{
		renderer.enabled = false;
	}
}