using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BombGame
{
	public class UpdateTiles
	{
#if UNITY_EDITOR
		[MenuItem("Tools/Update Tiles")]
		static void _UpdateTiles ()
		{
			Do (SelectionExtensions.GetSelected<_Tile>());
		}
#endif

		public static void Do (_Tile[] tiles)
		{
			for (int i = 0; i < tiles.Length; i ++)
			{
				_Tile tile = tiles[i];
				if (!tile.enabled)
				{
					tiles = tiles.RemoveAt(i);
					i --;
				}
				else
					tile.neighbors = new _Tile[0];
			}
			for (int i = 0; i < tiles.Length; i ++)
			{
				_Tile tile = tiles[i];
				for (int i2 = 0; i2 < tiles.Length; i2 ++)
				{
					_Tile tile2 = tiles[i2];
					if (i != i2 && _Tile.AreNeighbors(tile, tile2))
						tile.neighbors = tile.neighbors.Add(tile2);
				}
			}
			List<_Tile> _tiles = new List<_Tile>(tiles);
			List<_Tile[]> connectedTileGroups = new List<_Tile[]>();
			List<_Tile[]> supportingTileGroups = new List<_Tile[]>();
			List<_Tile> remainingTiles = new List<_Tile>(tiles);
			while (remainingTiles.Count > 0)
			{
				_Tile[] connectedTileGroup = _Tile.GetConnectedGroup(remainingTiles[0]);
				connectedTileGroups.Add(connectedTileGroup);
				List<_Tile> supportingTiles = new List<_Tile>();
				for (int i = 0; i < connectedTileGroup.Length; i ++)
				{
					_Tile connectedTile = connectedTileGroup[i];
					if (connectedTile.isSupportingTile)
						supportingTiles.Add(connectedTile);
				}
				for (int i = 0; i < connectedTileGroup.Length; i ++)
				{
					_Tile connectedTile = connectedTileGroup[i];
					remainingTiles.Remove(connectedTile);
				}
				supportingTileGroups.Add(supportingTiles.ToArray());
			}
			for (int i = 0; i < connectedTileGroups.Count; i ++)
			{
				_Tile[] connectedTileGroup = connectedTileGroups[i];
				_Tile[] supportingTileGroup = supportingTileGroups[i];
				for (int i2 = 0; i2 < connectedTileGroup.Length; i2 ++)
				{
					_Tile tile = connectedTileGroup[i2];
					tile.supportingTiles = supportingTileGroup;
#if UNITY_EDITOR
					tile.enabled = !tile.enabled;
					tile.enabled = !tile.enabled;
#endif
				}
			}
		}
	}
}
