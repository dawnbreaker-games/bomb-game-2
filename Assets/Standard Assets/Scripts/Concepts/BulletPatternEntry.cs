using System;
using UnityEngine;

namespace BombGame
{
	[Serializable]
	public class BulletPatternEntry
	{
		public string name;
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public Transform spawner;
		
		public Bullet[] Shoot ()
		{
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}
		
		public Bullet[] Shoot (Collider2D dontCollideWith)
		{
			Bullet[] output = bulletPattern.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				Physics2D.IgnoreCollision(bullet.collider, dontCollideWith, true);
				bullet.dontCollideWith = dontCollideWith;
				bullet.collider.enabled = true;
			}
			return output;
		}
	}
}