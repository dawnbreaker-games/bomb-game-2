using PlayerIOClient;

namespace Extensions
{
	public static class DatabaseExtensions
	{
		public static T GetOrMakeValue<T> (this DatabaseObject dbObj, string valueName, T defaultValue = default(T))
		{
			object value = dbObj.GetValue(valueName);
			if (value == null)
				return defaultValue;
			else
			{
				if (value is uint)
					dbObj.Set(valueName, (uint) value);
				else if (value is int)
					dbObj.Set(valueName, (int) value);
				else if (value is long)
					dbObj.Set(valueName, (long) value);
				else if (value is float)
					dbObj.Set(valueName, (float) value);
				else if (value is double)
					dbObj.Set(valueName, (double) value);
				else if (value is string)
					dbObj.Set(valueName, (string) value);
				else if (value is DatabaseArray)
					dbObj.Set(valueName, (DatabaseArray) value);
				else// if (value is DatabaseObject)
					dbObj.Set(valueName, (DatabaseObject) value);
				return (T) value;
			}
		}
	}
}