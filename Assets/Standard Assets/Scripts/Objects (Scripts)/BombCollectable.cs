using Extensions;
using UnityEngine;

namespace BombGame
{
	public class BombCollectable : MonoBehaviour
	{
		public Bomb bombPrefab;

		void Awake ()
		{
			if (PlayerPrefsExtensions.GetBool(bombPrefab.name + " owned", false))
				Destroy(gameObject);
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			PlayerPrefsExtensions.SetBool (bombPrefab.name + " owned", true);
			Player.instance.currentBombIndicators[Player.instance.currentBombIndex].SetActive(true);
			Destroy(gameObject);
		}
	}
}