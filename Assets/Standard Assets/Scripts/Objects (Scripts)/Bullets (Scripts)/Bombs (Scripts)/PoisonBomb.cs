using Extensions;
using UnityEngine;
using System.Collections;

namespace BombGame
{
	public class PoisonBomb : Bomb
	{
		public float poisonCloudDuration;
		public Explosion poisonCloud;

		public override void Start ()
		{
			base.Start ();
			if ((Match.localPlayer == null || Match.localPlayer == owner) && !owner.canHurtSelf)
			{
				poisonCloud.killZoneSpriteRenderer.material = poisonCloud.windZoneMaterial;
				poisonCloud.killZoneSpriteRenderer.color = explosionColorIfMadeByLocalPlayerAndCantHurtLocalPlayer;
			}
		}

		public override void DoUpdate ()
		{
			if (explosion.trs != null && explosion.trs.localScale.x < explodeSize)
			{
				explosion.trs.localScale += Vector3.one * explodeSize * Time.deltaTime / explodeDuration;
				explosion.killZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
				explosion.windZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
			}
			else
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
				poisonCloud.trs.SetParent(null);
				poisonCloud.trs.localScale = Vector3.one * explodeSize * explosion.minRadiusFractionToNotKill;
				poisonCloud.gameObject.SetActive(true);
				Destroy(poisonCloud.gameObject, poisonCloudDuration);
				Destroy(gameObject, poisonCloudDuration);
				Destroy(explosion.gameObject);
				Destroy(explosion.spriteMaskTrs.gameObject);
			}
		}
	}
}