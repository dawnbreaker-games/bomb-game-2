﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace BombGame
{
	public class Bomb : Bullet, IUpdatable
	{
		public Transform lifetimeTextTrs;
		public _Text lifetimeText;
		public Explosion explosion;
		public float explodeSize;
		public float explodeDuration;
		public Animator anim;
		public bool explodeOnHit;
		public bool canExplode;
		[HideInInspector]
		public Player owner;
		[HideInInspector]
		public float explodeTime;
		[HideInInspector]
		public List<Portal> startedInPortals = new List<Portal>();
		public float radius;
		public Color explosionColorIfMadeByLocalPlayerAndCantHurtLocalPlayer;
		[HideInInspector]
		public bool PlayerOwns
		{
			get
			{
				return Match.localPlayer != null || Survival.instance != null || PlayerPrefsExtensions.GetBool(name + " owned", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool (name + " owned", value);
			}
		}
		LifetimeUpdater lifetimeUpdater;

		public override void OnEnable ()
		{
			instances.Add(this);
		}

		public virtual void Start ()
		{
			if (lifetime > 0)
			{
				lifetimeUpdater = new LifetimeUpdater(this);
				GameManager.updatables = GameManager.updatables.Add(lifetimeUpdater);
			}
			SceneManager.sceneLoaded += OnSceneLoaded;
			if ((Match.localPlayer == null || Match.localPlayer == owner) && !owner.canHurtSelf)
			{
				explosion.killZoneSpriteRenderer.material = explosion.windZoneMaterial;
				explosion.killZoneSpriteRenderer.color = explosionColorIfMadeByLocalPlayerAndCantHurtLocalPlayer;
			}
		}

		public override void OnTriggerEnter2D (Collider2D other)
		{
		}

		public virtual void OnCollisionEnter2D (Collision2D coll)
		{
			if (explodeOnHit)
				Explode ();
			explodeOnHit = false;
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
			GameManager.updatables = GameManager.updatables.Remove(this);
			GameManager.updatables = GameManager.updatables.Remove(lifetimeUpdater);
		}

		public virtual void Explode ()
		{
			gameObject.SetActive(false);
			spriteRenderer.enabled = false;
			explosion.trs.localScale = Vector3.zero;
			explosion.trs.SetParent(null);
			explosion.gameObject.SetActive(true);
			explodeTime = Time.time;
			GameManager.updatables = GameManager.updatables.Add(this);
			GameManager.updatables = GameManager.updatables.Remove(lifetimeUpdater);
		}

		public virtual void DoUpdate ()
		{
			if (explosion.trs != null && explosion.trs.localScale.x < explodeSize)
			{
				explosion.trs.localScale += Vector3.one * explodeSize * Time.deltaTime / explodeDuration;
				explosion.killZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
				explosion.windZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
			}
			else
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
				Destroy(gameObject);
				Destroy(explosion.gameObject);
				Destroy(explosion.spriteMaskTrs.gameObject);
			}
		}

		public override void Death (Player killer)
		{
			if (gameObject.activeSelf)
				Explode ();
		}
		
		public override void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			base.OnSceneLoaded (scene, loadMode);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		class LifetimeUpdater : IUpdatable
		{
			Bomb bomb;

			public LifetimeUpdater (Bomb bomb)
			{
				this.bomb = bomb;
			}

			public void DoUpdate ()
			{
				bomb.lifetime -= Time.deltaTime;
				bomb.lifetimeTextTrs.eulerAngles = Vector3.zero;
				bomb.lifetimeText.Text = bomb.lifetime.ToString("F1");
				if (bomb.lifetime <= 0)
					bomb.Explode ();
			}
		}
	}
}