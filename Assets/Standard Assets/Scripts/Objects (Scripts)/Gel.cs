using UnityEngine;
using System.Collections.Generic;

namespace BombGame
{
	public class Gel : _Tile
	{
		public float addToDrag;
		public float addToMultipyVelocityChanges;
		static List<Rigidbody2D> rigidbodiesInsideMe = new List<Rigidbody2D>();

		void OnTriggerEnter2D (Collider2D other)
		{
			Rigidbody2D rigid = other.GetComponent<Rigidbody2D>();
			if (!rigidbodiesInsideMe.Contains(rigid))
			{
				Player player = other.GetComponent<Player>();
				if (player != null)
					player.multiplyVelocityChanges += addToMultipyVelocityChanges;
				rigid.drag += addToDrag;
			}
			rigidbodiesInsideMe.Add(rigid);
		}

		void OnTriggerExit2D (Collider2D other)
		{
			Rigidbody2D rigid = other.GetComponent<Rigidbody2D>();
			rigidbodiesInsideMe.Remove(rigid);
			if (!rigidbodiesInsideMe.Contains(rigid))
			{
				Player player = other.GetComponent<Player>();
				if (player != null)
					player.multiplyVelocityChanges -= addToMultipyVelocityChanges;
				rigid.drag -= addToDrag;
			}
		}
	}
}