using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace BombGame
{
	public class SavePoint : MonoBehaviour
	{
		public bool Found
		{
			get 
			{
				return PlayerPrefsExtensions.GetBool(name + " found", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool (name + " found", value);
			}
		}
		public static List<SavePoint> instances = new List<SavePoint>();
		public WorldMapIcon worldMapIcon;
		[HideInInspector]
		public Vector2 initPosition;

		void Awake ()
		{
			instances.Add(this);
		}

		void OnDestroy ()
		{
			instances.Remove(this);
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			initPosition = GetComponent<Transform>().position;
		}
#endif

		void OnTriggerEnter2D (Collider2D other)
		{
			Player.instance.SavedPosition = initPosition;
#if !UNITY_WEBGL
			WorldMap.savePointPlayerIsAt = this;
			if (WorldMap.foundNewCellSinceSaved)
			{
				bool[] exploredCellPositions = new bool[WorldMap.exploredCellPositions.Length];
				WorldMap.exploredCellPositions.CopyTo(exploredCellPositions, 0);
				WorldMap.ExploredCellPositions = exploredCellPositions;
			}
			Found = true;
			SaveAndLoadManager.Save ();
			WorldMap.foundNewCellSinceSaved = false;
#endif
		}

		void OnTriggerExit2D (Collider2D other)
		{
			WorldMap.savePointPlayerIsAt = null;
		}
	}
}