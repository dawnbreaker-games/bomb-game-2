using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class End : _Tile, ICollisionEnterHandler2D
	{
		public Collider2D collider;
		public Collider2D Collider
		{
			get
			{
				return collider;
			}
		}

		public void OnCollisionEnter2D (Collision2D coll)
		{
			Player player = coll.gameObject.GetComponentInParent<Player>();
			if (player != null)
				_SceneManager.instance.NextScene ();
		}
	}
}