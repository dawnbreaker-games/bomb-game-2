﻿using Extensions;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
// using DelayedDespawn = BombGame.ObjectPool.DelayedDespawn;

namespace BombGame
{
	public class Bullet : Spawnable, IDestructable
	{
		public float Hp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public uint MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public Rigidbody2D rigid;
		public Collider2D collider;
		public float moveSpeed;
		public float lifetime;
		public SpriteRenderer spriteRenderer;
		public Color canRetargetColor;
		public Color cantRetargetColor;
		public bool despawnOnHitDestructable;
		public bool despawnOnHitNonDestructable;
		[HideInInspector]
		public int timesRetargeted;
		[HideInInspector]
		public bool hasFinishedRetargeting;
		public float damageAmountOnHitDestructable = 1;
		[HideInInspector]
		public Collider2D dontCollideWith;
		// DelayedDespawn delayedDespawn;
		public static List<Bullet> instances = new List<Bullet>();

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (collider == null)
					collider = GetComponent<Collider2D>();
				return;
			}
#endif
			rigid.velocity = trs.up * moveSpeed;
			if (lifetime > 0)
				Destroy(gameObject, lifetime);
			SceneManager.sceneLoaded += OnSceneLoaded;
			instances.Add(this);
			// delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, lifetime);
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			IDestructable destructable = other.GetComponent<IDestructable>();
			if (destructable != null)
			{
				destructable.TakeDamage (damageAmountOnHitDestructable, null);
				if (despawnOnHitDestructable)
					Destroy(gameObject);
					// Despawn ();
			}
			else if (despawnOnHitNonDestructable)
			{
				Explosion explosion = other.GetComponent<Explosion>();
				if (explosion == null)
				{
					ElectricMist electricMist = other.GetComponent<ElectricMist>();
					if (electricMist == null)
						Destroy(gameObject);
						// Despawn ();
				}
			}
		}

		// public virtual void Despawn ()
		// {
		// 	ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
		// 	ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		// }

		public void TakeDamage (float amount, Player attacker)
		{
			Death (attacker);
		}

		public virtual void Death (Player killer)
		{
			if (this != null)
				Destroy(gameObject);
				// Despawn ();
		}

		public virtual void Retarget (Vector2 direction, bool lastRetarget = false)
		{
			if (hasFinishedRetargeting)
				return;
			trs.up = direction;
			rigid.velocity = trs.up * moveSpeed;
			timesRetargeted ++;
			if (lastRetarget)
			{
				hasFinishedRetargeting = true;
				spriteRenderer.color = cantRetargetColor;
				spriteRenderer.sortingOrder --;
			}
		}

		public virtual void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			SceneManager.sceneLoaded -= OnSceneLoaded;
			timesRetargeted = 0;
			if (hasFinishedRetargeting)
			{
				hasFinishedRetargeting = false;
				spriteRenderer.color = canRetargetColor;
				spriteRenderer.sortingOrder ++;
			}
			if (dontCollideWith != null)
				Physics2D.IgnoreCollision(collider, dontCollideWith, false);
			dontCollideWith = null;
			instances.Remove(this);
		}
		
		public virtual void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			if (this != null)
				Destroy(gameObject);
		}
	}
}
