using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class Explosion : Hazard
	{
		public float windSpeed;
		[Range(0, 1)]
		public float minRadiusFractionToNotKill;
		public float maxRadius;
		public Bomb cameFrom;
		public Transform spriteMaskTrs;
		public SpriteMask spriteMask;
		public ShortRange sortingOrderRange;
		public SpriteRenderer killZoneSpriteRenderer;
		public SpriteRenderer windZoneSpriteRenderer;
		public Material killZoneMaterial;
		public Material windZoneMaterial;
		public bool damageOverTime;
		List<IDestructable> hitDestructables = new List<IDestructable>();

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (!Application.isPlaying)
			{
				trs.SetWorldScale (Vector3.one * maxRadius * 2);
				spriteMaskTrs.SetWorldScale (Vector3.one * maxRadius * minRadiusFractionToNotKill * 2);
			}
		}
#endif

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			spriteMaskTrs.SetParent(null);
			spriteMaskTrs.gameObject.SetActive(true);
			short sortingOrder = sortingOrderRange.Get(Random.value);
			killZoneMaterial = new Material(killZoneMaterial);
			windZoneMaterial = new Material(windZoneMaterial);
			killZoneSpriteRenderer.sharedMaterial = killZoneMaterial;
			windZoneSpriteRenderer.sharedMaterial = windZoneMaterial;
			killZoneSpriteRenderer.sortingOrder = sortingOrder;
			windZoneSpriteRenderer.sortingOrder = sortingOrder;
			spriteMask.frontSortingOrder = sortingOrder + 1;
			spriteMask.backSortingOrder = sortingOrder - 1;
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			OnTriggerStay2D (other);
		}

		void OnTriggerStay2D (Collider2D other)
		{
			if (minRadiusFractionToNotKill == 1)
				ApplyDamage (other);
			else
			{
				Player player = other.GetComponent<Player>();
				if (player != null)
					player.velocityEffectors_Vector2Dict["Wind"].value = (player.trs.position - trs.position).normalized * windSpeed;
				else
				{
					Bullet bullet = other.GetComponent<Bullet>();
					if (bullet != null)
					{
						if (bullet is Bomb)
							bullet.rigid.velocity = (bullet.trs.position - trs.position).normalized * windSpeed;
						else
							bullet.Retarget (bullet.trs.position - trs.position);
					}
					else
					{
						Enemy_Sandworm enemySandworm = other.GetComponent<Enemy_Sandworm>();
						if (enemySandworm != null)
							enemySandworm.extraVelocity = (enemySandworm.trs.position - trs.position).normalized * windSpeed;
					}
				}
				float minRadiusToNotKill = maxRadius * minRadiusFractionToNotKill;
				Transform otherTrs = other.GetComponent<Transform>();
				PolygonCollider2D polygonCollider2D = other as PolygonCollider2D;
				if (polygonCollider2D != null)
				{
					for (int i = 0; i < polygonCollider2D.points.Length; i ++)
					{
						Vector2 point = otherTrs.TransformPoint(polygonCollider2D.points[i]);
						if ((point - (Vector2) trs.position).sqrMagnitude < minRadiusToNotKill * minRadiusToNotKill)
						{
							ApplyDamage (other);
							break;
						}
					}
				}
				else
				{
					CircleCollider2D circleCollider2D = other as CircleCollider2D;
					if (circleCollider2D != null)
					{
						minRadiusToNotKill += circleCollider2D.radius;
						if ((otherTrs.position - trs.position).sqrMagnitude < minRadiusToNotKill * minRadiusToNotKill)
							ApplyDamage (other);
					}
					else
					{
						BoxCollider2D boxCollider2D = (BoxCollider2D) other;
						if (((Vector2) boxCollider2D.bounds.min - (Vector2) trs.position).sqrMagnitude < minRadiusToNotKill * minRadiusToNotKill || ((Vector2) boxCollider2D.bounds.max - (Vector2) trs.position).sqrMagnitude < minRadiusToNotKill * minRadiusToNotKill || (new Vector2(boxCollider2D.bounds.min.x, boxCollider2D.bounds.max.y) - (Vector2) trs.position).sqrMagnitude < minRadiusToNotKill * minRadiusToNotKill || (new Vector2(boxCollider2D.bounds.max.x, boxCollider2D.bounds.min.y) - (Vector2) trs.position).sqrMagnitude < minRadiusToNotKill * minRadiusToNotKill)
							ApplyDamage (other);
					}
				}
			}
		}

		void ApplyDamage (Collider2D collider)
		{
			IDestructable destructable = collider.GetComponent<IDestructable>();
			if (destructable == null)
				return;
			if (damageOverTime)
				ApplyDamage (destructable, damage * Time.deltaTime);
			else if (!hitDestructables.Contains(destructable))
			{
				hitDestructables.Add(destructable);
				ApplyDamage (destructable, damage);
			}
		}

		public void DestroyMe ()
		{
			Destroy(gameObject);
		}
		
		public override void ApplyDamage (IDestructable destructable, float amount)
		{
			destructable.TakeDamage (amount, cameFrom.owner);
		}
	}
}