#if UNITY_EDITOR
using System.Collections.Generic;

namespace BombGame
{
	public class PreBuildScript : EditorScript
	{
		public static List<PreBuildScript> instances = new List<PreBuildScript>();

		public override void OnEnable ()
		{
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			instances.Remove(this);
		}
	}
}
#else
namespace BombGame
{
	public class PreBuildScript : EditorScript
	{
	}
}
#endif
