#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	[ExecuteInEditMode]
	public class CombineTiles : EditorScript
	{
		public bool checkForNeighbors;

		public override void Do ()
		{
			_Tile[] tiles = SelectionExtensions.GetSelected<_Tile>();
			if (!checkForNeighbors)
			{
				for (int i = 0; i < tiles.Length; i ++)
				{
					_Tile tile = tiles[i];
					
				}
			}
			else
			{
				
			}
		}
	}
}
#else
using UnityEngine;

namespace BombGame
{
	public class CombineTiles : EditorScript
	{
	}
}
#endif