#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace BombGame
{
	public class ReplaceWithTile : EditorScript
	{
		public Transform trs;
		public _Tile replaceWithTilePrefab;

		public override void Do ()
		{
			if (this == null)
				return;
			if (trs == null)
				trs = GetComponent<Transform>();
			if (replaceWithTilePrefab != null)
			{
				_Tile tile = (_Tile) PrefabUtility.InstantiatePrefab(replaceWithTilePrefab);
				tile.trs.position = trs.position;
				tile.trs.rotation = trs.rotation;
				tile.trs.SetParent(trs.parent);
				tile.trs.localScale = trs.localScale;
				GameManager.DestroyOnNextEditorUpdate (gameObject);
			}
		}
	}
}
#else
namespace BombGame
{
	public class ReplaceWithTile : EditorScript
	{
	}
}
#endif