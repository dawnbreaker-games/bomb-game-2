using System;
using PlayerIO.GameLibrary;
using System.Collections.Generic;

public class Match
{
	public virtual string GameModeName
	{
		get
		{
			throw new NotImplementedException();
		}
	}
	float makeVulnerableDelay;
	uint minPlayers;
	uint maxPlayers;
	uint spawnPointCount;
	uint minTimeWithNoScoreChangeToEndRound;
	bool ranked;
	byte maxScore;
	List<uint> spawnPointsRemaining = new List<uint>();
	uint[] spawnPoints = new uint[0];
	Game<Player> game;
	List<uint> unusedPlayerIds = new List<uint>();
	Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
	List<Player> allPlayers = new List<Player>();
	Timer endRoundTimer;
	uint currentRound;
	bool hasEnded;
	Random random = new Random();
	const uint MAX_COLOR_TRIES = 10;
	const float MIN_COLOR_SIMULARITY = .15f;

	public Match (Game<Player> game)
	{
		this.game = game;
		makeVulnerableDelay = float.Parse(game.RoomData["makeVulnerableDelay"]);
		minPlayers = uint.Parse(game.RoomData["minPlayers"]);
		maxPlayers = uint.Parse(game.RoomData["maxPlayers"]);
		for (uint i = 0; i < maxPlayers; i ++)
			unusedPlayerIds.Add(i);
		spawnPointCount = uint.Parse(game.RoomData["spawnPointCount"]);
		spawnPoints = new uint[spawnPointCount];
		for (uint i = 0; i < spawnPointCount; i ++)
			spawnPoints[i] = i;
		spawnPointsRemaining = new List<uint>(spawnPoints);
		minTimeWithNoScoreChangeToEndRound = uint.Parse(game.RoomData["minTimeWithNoScoreChangeToEndRound"]);
		ranked = bool.Parse(game.RoomData["ranked"]);
		maxScore = byte.Parse(game.RoomData["maxScore"]);
		game.PreloadPlayerObjects = true;
	}

	public void UserJoined (Player player)
	{
		uint playerId = unusedPlayerIds[0];
		allPlayers.Add(player);
		foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
		{
			uint usedPlayerId = keyValuePair.Key;
			Player player2 = keyValuePair.Value;
			player.Send("Spawn Player", usedPlayerId, player2.color.x, player2.color.y, player2.color.z, (uint) 0);
			player.Send("Move Player", usedPlayerId, player2.position.x, player2.position.y);
			if (!player2.invulnerable)
				player.Send("Make Player Vulnerable", usedPlayerId);
		}
		int spawnPointIndex = random.Next(spawnPointsRemaining.Count);
		Vector3 teamColor = new Vector3();
		float leastColorSimularity = float.MaxValue;
		Vector3 leastSimularColor = new Vector3();
		bool isValidColor = true;
		for (int i = 0; i < MAX_COLOR_TRIES; i ++)
		{
			isValidColor = true;
			teamColor = new Vector3((float) random.NextDouble(), (float) random.NextDouble(), (float) random.NextDouble());
			foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
			{
				float colorSimularity = 1f - Vector3.GetDifference(keyValuePair.Value.color, teamColor);
				if (colorSimularity < MIN_COLOR_SIMULARITY)
				{
					if (colorSimularity < leastColorSimularity)
					{
						leastColorSimularity = colorSimularity;
						leastSimularColor = teamColor;
					}
					isValidColor = false;
					break;
				}
			}
			if (isValidColor)
				break;
		}
		if (!isValidColor)
			teamColor = leastSimularColor;
		player.color = teamColor;
		uint spawnPoint = spawnPointsRemaining[spawnPointIndex];
		Message spawnPlayerMessage = Message.Create("Spawn Player", playerId, teamColor.x, teamColor.y, teamColor.z, spawnPoint);
		Message spawnLocalPlayerMessage = Message.Create("Spawn Player", playerId, teamColor.x, teamColor.y, teamColor.z, spawnPoint, true);
		spawnPointsRemaining.RemoveAt(spawnPointIndex);
		foreach (Player player2 in game.Players)
		{
			if (player != player2)
				player2.Send(spawnPlayerMessage);
			else
			{
				player.Send(spawnLocalPlayerMessage);
				for (uint i = 0; i < currentRound; i ++)
					player.Send("End Round");
			}
		}
		unusedPlayerIds.RemoveAt(0);
		player.id = playerId;
		playersDict.Add(playerId, player);
		game.ScheduleCallback(() => { MakePlayerVulnerable (player); }, (int) (makeVulnerableDelay * 1000));
		if (game.PlayerCount >= Math.Max(minPlayers, 2))
		{
			spawnPointsRemaining = new List<uint>(spawnPoints);
			if (endRoundTimer == null)
				endRoundTimer = game.AddTimer(EndRound, (int) minTimeWithNoScoreChangeToEndRound * 1000);
		}
	}

	public void UserLeft (Player player)
	{
		playersDict.Remove(player.id);
		if (ranked && playersDict.Count == 1 && !hasEnded)
		{
			foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
				End (keyValuePair.Value);
		}
		unusedPlayerIds.Add(player.id);
		if (playersDict.Count < Math.Max(minPlayers, 2) && endRoundTimer != null)
		{
			endRoundTimer.Stop();
			endRoundTimer = null;
		}
		foreach (Player player2 in game.Players)
		{
			if (player != player2)
				player2.Send("Remove Player", player.id);
		}
	}

	public void GotMessage (Player player, Message message)
	{
		if (message.Type == "Move Player")
		{
			player.position = new Vector2(message.GetFloat(0), message.GetFloat(1));
			Message movePlayerMessage = Message.Create("Move Player", player.id, player.position.x, player.position.y);
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(movePlayerMessage);
			}
		}
		else if (message.Type == "Shoot Bomb")
		{
			Message shootBombMessage = Message.Create("Shoot Bomb", player.id, message.GetUInt(0), message.GetFloat(1), message.GetFloat(2));
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(shootBombMessage);
			}
		}
		else if (message.Type == "Revive Player")
		{
			Message revivePlayerMessage = Message.Create("Revive Player", player.id, message.GetUInt(0));
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(revivePlayerMessage);
			}
			player.invulnerable = true;
			game.ScheduleCallback(() => { MakePlayerVulnerable (player); }, (int) (makeVulnerableDelay * 1000));
		}
		else
		{
			message.Add(player.id);
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(message);
			}
			if (message.Type == "Kill Player")
			{
				uint killerId = message.GetUInt(0);
				Player killer = playersDict[killerId];
				if (killer != player)
				{
					killer.score ++;
					if (killer.score >= maxScore && !hasEnded)
						End (killer);
				}
				else
					player.score --;
				endRoundTimer.Stop();
				endRoundTimer = game.AddTimer(EndRound, (int) minTimeWithNoScoreChangeToEndRound * 1000);
				if (ranked)
				{
					DatabaseObject victimDBObject = player.PlayerObject;
					DatabaseArray dbArray = victimDBObject.GetArray("deaths");
					dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
					victimDBObject.Set("deaths", dbArray);
                    dbArray = victimDBObject.GetArray("deathsInCurrentSeason");
                    dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
                    victimDBObject.Set("deathsInCurrentSeason", dbArray);
					victimDBObject.Save();
					DatabaseObject killerDBObject = killer.PlayerObject;
					dbArray = killerDBObject.GetArray("kills");
					dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
					killerDBObject.Set("kills", dbArray);
                    dbArray = killerDBObject.GetArray("killsInCurrentSeason");
                    dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
                    killerDBObject.Set("killsInCurrentSeason", dbArray);
					killerDBObject.Save();
				}
			}
		}
	}

	void MakePlayerVulnerable (Player player)
	{
		player.invulnerable = false;
		foreach (Player player2 in game.Players)
			player2.Send("Make Player Vulnerable", player.id);
	}

	void End (Player winner)
	{
		hasEnded = true;
		if (ranked)
		{
			List<Player> losingPlayers = new List<Player>(allPlayers);
			losingPlayers.Remove(winner);
            DatabaseObject winnerDBObject = winner.PlayerObject;
            DatabaseArray dbArray = winnerDBObject.GetArray("wins");
            dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
            winnerDBObject.Set("wins", dbArray);
			dbArray = winnerDBObject.GetArray("winsInCurrentSeason");
            dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
            winnerDBObject.Set("winsInCurrentSeason", dbArray);
			dbArray = winnerDBObject.GetArray("skill");
            dbArray.Set(GameModeName, dbArray.GetFloat(GameModeName) + 1);
            winnerDBObject.Set("skill", dbArray);
			dbArray = winnerDBObject.GetArray("skillInCurrentSeason");
            dbArray.Set(GameModeName, dbArray.GetFloat(GameModeName) + 1);
            winnerDBObject.Set("skillInCurrentSeason", dbArray);
            winnerDBObject.Save();
            for (int i = 0; i < losingPlayers.Count; i ++)
			{
				Player loser = losingPlayers[i];
				DatabaseObject loserDBObject = loser.PlayerObject;
                dbArray = loserDBObject.GetArray("losses");
                dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
                loserDBObject.Set("losses", dbArray);
				dbArray = loserDBObject.GetArray("lossesInCurrentSeason");
                dbArray.Set(GameModeName, dbArray.GetUInt(GameModeName) + 1);
                loserDBObject.Set("lossesInCurrentSeason", dbArray);
				float skillChange;
				if (loser.score >= 0)
					skillChange = 1f - ((float) loser.score / maxScore);
				else
					skillChange = 1f + ((float) Math.Abs(loser.score) / maxScore);
				dbArray = loserDBObject.GetArray("skill");
                dbArray.Set(GameModeName, dbArray.GetFloat(GameModeName) - skillChange);
                loserDBObject.Set("skill", dbArray);
				dbArray = loserDBObject.GetArray("skillInCurrentSeason");
                dbArray.Set(GameModeName, dbArray.GetFloat(GameModeName) - skillChange);
                loserDBObject.Set("skillInCurrentSeason", dbArray);
				loserDBObject.Save();
			}
		}
		DisconnectPlayers ();
	}

	void DisconnectPlayers ()
	{
		List<Player> players = new List<Player>(playersDict.Values);
		for (int i = 0; i < players.Count; i ++)
		{
			Player player = players[i];
			player.Disconnect();
		}
	}

	void EndRound ()
	{
		foreach (Player player in game.Players)
			player.Send("End Round");
		currentRound ++;
	}
}