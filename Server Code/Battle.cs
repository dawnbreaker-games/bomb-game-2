using PlayerIO.GameLibrary;

[RoomType("Battle")]
public class Battle : Game<Player>
{
	BattleMatch match;

	public override void GameStarted ()
	{
		match = new BattleMatch(this);
	}

	public override void UserJoined (Player player)
	{
		match.UserJoined (player);
	}

	public override void UserLeft (Player player)
	{
		match.UserLeft (player);
	}

	public override void GotMessage (Player player, Message message)
	{
		match.GotMessage (player, message);
	}
}