using PlayerIO.GameLibrary;

public class BattleMatch : Match
{
    public override string GameModeName => "Battle";

    public BattleMatch (Game<Player> game) : base (game)
	{
	}
}