using PlayerIO.GameLibrary;

public class SoccerMatch : Match
{
    public override string GameModeName => "Soccer";

    public SoccerMatch (Game<Player> game) : base (game)
	{
	}
}