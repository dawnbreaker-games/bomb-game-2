using PlayerIO.GameLibrary;

public class Player : BasePlayer
{
	public Vector2 position;
	public uint id;
	public bool invulnerable = true;
	public int score;
	public Vector3 color;
}