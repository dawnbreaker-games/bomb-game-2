using PlayerIO.GameLibrary;

[RoomType("Soccer")]
public class Soccer : Game<Player>
{
	SoccerMatch match;

	public override void GameStarted ()
	{
		match = new SoccerMatch(this);
	}

	public override void UserJoined (Player player)
	{
		match.UserJoined (player);
	}

	public override void UserLeft (Player player)
	{
		match.UserLeft (player);
	}

	public override void GotMessage (Player player, Message message)
	{
		match.GotMessage (player, message);
	}
}